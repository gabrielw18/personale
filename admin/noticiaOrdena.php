<?php
require 'inc/protect.php';
$thisPage = 'noticias';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Ordenar Imagens</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>

                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <?php
                if (!empty($_GET['id']) and ctype_digit((string) $_GET['id'])) {
                    $sql = "SELECT id FROM noticias WHERE id = " . $_GET['id'];
                    $resultado = $content->sql($sql);
                    if ($resultado) {
                        $num_rows = $content->num_rows($resultado);
                        if ($num_rows > 0) {
                            $row = $content->fetch($resultado);
                            ?>
                            <label><h4>Galeria de Fotos</h4> <br><br></label>
                                <ul id="page_list">
                                    <?php
                                    $sql_fotos = "select id, imagem, ordem from imagens where noticia = " . $row['id'] . " ORDER BY ordem";
                                    $resultado_fotos = $content->sql($sql_fotos);
                                    if ($resultado_fotos) {
                                        $num_rows_fotos = $content->num_rows($resultado_fotos);
                                        if ($num_rows_fotos > 0) {
                                            while ($row_fotos = $content->fetch($resultado_fotos)) {
                                                ?>
                                                <li id="<?=$row_fotos['id']?>">
                                                    <img src="../img/upload/thumb/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="shadow_2"/>
                                                </li>
                                                <?php
                                            }
                                        } else {
                                            echo "Nenhuma imagem cadastrada.";
                                        }
                                    } else {
                                        echo "Nenhuma imagem encontrada.";
                                    }
                                    ?>
                                </ul>
                                    <div class="clearfix"></div><br>
                            <?php
                        } else {
                            echo "<div class='text-center'>Cadastro não encontrado.</div>";
                        }
                    } else {
                        echo "<div class='text-center'>Cadastro não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>ja -Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/moment-with-locales.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/jquery.mask.min.js"></script>
        <script type="text/javascript" src="TinyMCE/tinymce.min.js"></script>
        <script type="text/javascript">

            $('.pg-<?=$thisPage?>').addClass('active');

            $(document).ready(function(){
             $( "#page_list" ).sortable({
              placeholder : "ui-state-highlight",
              update  : function(event, ui)
              {
               var page_id_array = new Array();
               $('#page_list li').each(function(){
                page_id_array.push($(this).attr("id"));
               });
               $.ajax({
                url:"ajax/noticiaordena.php",
                method:"POST",
                data:{page_id_array:page_id_array},
               });
              }
             });

            });


        </script>
    </body>
</html>