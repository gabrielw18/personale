<?php

require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

function json_message($type_message, $text_message){
    $output = json_encode(
        array(
            'type'=> $type_message,
            'text' => $text_message
        )
    );
    die($output);
}

// Verifica se está vido do AJAX
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    header("Location:../../home");
    die();
}

//ENVIA CONTATO
if (isset($_GET["action"]) and $_GET["action"] === "contato") {
    require "../../inc/class.smtp.php";
    require "../../inc/class.phpmailer.php";
    $mail = new PHPMailer;

    @$nome = $_POST["nome"];
    @$email = $_POST["email"];
    @$telefone = $_POST["telefone"];
    @$mensagem = $_POST["mensagem"];

    if (empty($nome)) {
        json_message("error", "Preencha seu nome");
    }

     if (empty($telefone)) {
        json_message("error", "Preencha seu telefone");
    }

    if (empty($email)) {
        json_message("error", "Preencha seu e-mail");
    }

    if (!empty($email) and ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        json_message("error", "Preencha o e-mail corretamente");
    }

    if (empty($mensagem)) {
        json_message("error", "Preencha uma mensagem");
    }

    $sql_infos = "select email from infos where id = 1";
    $resultado_infos = $content->sql($sql_infos);
    if ($resultado_infos) {
        $num_rows_infos = $content->num_rows($resultado_infos);
        if ($num_rows_infos > 0) {
            $row_infos = $content->fetch($resultado_infos);

            $message = '<html>
                        <body>
                        <span style="font-family: tahoma">Um usuário visitou o seu site e deixou uma mensagem na página de <em>contato</em>. Seguem os dados:</span><br><br>
                        <table style="border-spacing: 0; border-collapse: collapse; max-width: 960px; font-family: Tahoma">
                            <tbody>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">Data</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; background-color: #eee; font-family: Tahoma">' . date('d/m/Y') . '</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">Hora</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; background-color: #eee; font-family: Tahoma">' . date('H:i') . '</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">Nome</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; font-family: Tahoma">' . stripslashes($nome) . '</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">Telefone</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; font-family: Tahoma">' . stripslashes($telefone) . '</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">E-mail</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; font-family: Tahoma">' . stripslashes($email) . '</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #fff; background-color: #000; font-family: Tahoma">Mensagem</td>
                                    <td style="border: 1px solid #333; padding: 5px 10px; color: #000; font-family: Tahoma">' . stripslashes($mensagem) . '</td>
                                </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>';

            $mail->setFrom($content->limpaEcho($content->decodificar($row_infos['email'])), 'Site: Contato');
            $mail->addAddress($content->limpaEcho($content->decodificar($row_infos['email'])), 'Site: Contato');

            $mail->Subject = 'Site: Contato';
            $mail->isHTML();
            $mail->Body = $message;
            $mail->CharSet = 'UTF-8';

            if ($mail->send()) {
                json_message("success", "Contato enviado com sucesso");
            } else {
                json_message("error", "Tente novamnete mais tarde");
            }
        } else {
            json_message("error", "Tente novamnete mais tarde");
        }
    } else {
        json_message("error", "Tente novamnete mais tarde");
    }
}