<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }

    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}

if (isset($_GET["action"]) and $_GET["action"] === "alter1") {
    @$endereco = $_POST["endereco"];
    @$telefone = $_POST["telefone"];
    @$telefones = $_POST["telefones"];
    @$email = $_POST["email"];
    @$horario = $_POST["horario"];
    @$facebook = $_POST["facebook"];
    @$linkedin = $_POST["linkedin"];
    @$instagram = $_POST["instagram"];
    @$youtube = $_POST["youtube"];
    @$iframe = $_POST["iframe"];


    if (empty($email) or ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Você deve informar um \"e-mail\" válido.";
        exit;
    }

    $sql = "UPDATE infos SET endereco = '" . $content->codificar($content->limpaInsert($endereco)) . "', telefone = '" . $content->codificar($content->limpaInsert($telefone)) . "', email = '" . $content->codificar($content->limpaInsert($email)) . "', horario = '" . $content->codificar($content->limpaInsert($horario)) . "', facebook = '" . $content->codificar($content->limpaInsert($facebook)) . "', linkedin = '" . $content->codificar($content->limpaInsert($linkedin)) . "', instagram = '" . $content->codificar($content->limpaInsert($instagram)) . "', youtube = '" . $content->codificar($content->limpaInsert($youtube)) . "', telefones = '" . $content->codificar($content->limpaInsert($telefones)) . "', iframe = '" . $purifier->purify($iframe) . "' WHERE id = 1";
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao atualizar informações. Tente novamente mais tarde.";
        exit;
    }
}
