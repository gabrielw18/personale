<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }
    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }
    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}


// NOVO
if (isset($_GET["action"]) and $_GET["action"] === "novo") {
    require "../inc/imagemanager.php";
    @$titulo = $_POST["titulo"];
    @$tipo = $_POST["tipo"];
    @$data = $_POST["data"];
    @$resumo = $_POST["resumo"];
    @$ativo = $_POST["ativo"];
    @$ordem = $_POST["ordem"];
    @$texto = $_POST["texto"];
    @$keywords = $_POST["keywords"];
    @$imagem = $_FILES["imagem"];


    if (!$content->checkImg($imagem)) {
        echo "Selecione uma \"imagem\" válida.";
        exit;
    }

    $move = $content->upload($imagem, "../../img/upload/");

    $imgr = new imagemanager("../../img/upload/" . $move);
    $thumb = $imgr->thumbnail(1200, 649)->save("../../img/upload/thumb/" . $move, 95);
    unset($imgr);

    unlink("../../img/upload/" . $move);


    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"título\" é obrigatório.";
        exit;
    }

    $datas = explode("/", $data);
    if (!checkdate($datas[1], $datas[0], $datas[2])) {
        echo "A \"data\" informada é inválida.";
        exit;
    }

    if (empty($data)) {
        echo "Campo \"data\" é obrigatório.";
        exit;
    }

    if (empty($purifier->purify($resumo))) {
        echo "Campo \"resumo\" é obrigatório.";
        exit;
    }

    if (empty($purifier->purify($texto))) {
        echo "Campo \"texto\" é obrigatório.";
        exit;
    }

    $data = $content->dataToDate($data);
    $w = date('D', strtotime($data));

    switch ($w) {
        case "Mon":
            $dw = "Segunda-feira";
            break;
        case "Tue":
            $dw = "Terça-feira";
            break;
        case "Wed":
            $dw = "Quarta-feira";
            break;
        case "Thu":
            $dw = "Quinta-feira";
            break;
        case "Fri":
            $dw = "Sexta-feira";
            break;
        case "Sat":
            $dw = "Sábado";
            break;
        case "Sun":
            $dw = "Domingo";
            break;
    }

    $sql = "INSERT INTO noticias (titulo, tipo, imagem, data, dia, texto, resumo, ordem, ativo, keywords) VALUES ('" . $content->codificar($content->limpaInsert($titulo)) . "', '" . $content->codificar($content->limpaInsert($tipo)) . "', '" . $content->codificar($move) . "', '" . $content->limpaInsert($data) . "', '" . $content->codificar($content->limpaInsert($dw)) . "', '" . $purifier->purify($texto) . "', '" . $purifier->purify($resumo) . "', '" . $purifier->purify($ordem) . "', '" . $purifier->purify($ativo) . "', '" . $purifier->purify($keywords) . "')";
    $resultado = $content->sql($sql);

    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
        exit;
    }
}


//ADD IMAGENS A GALERIA
if (isset($_GET["action"]) and $_GET["action"] === "img_novo") {
    require "../inc/imagemanager.php";
    $dir = '../../img/upload/';

    @$imagens = $_FILES['imagens'];

    if (!isset($imagens)) {
        echo "error";
        exit;
    }

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $names = $imagens['name'];
    $tmp_names = $imagens['tmp_name'];
    $types = $imagens['type'];

    $j = 1;
    $sql = "INSERT INTO imagens (imagem, noticia) VALUES ";

    for ($i = 0; $i < count($tmp_names); $i++) {
        if (preg_match("/^image\/(jpeg|jpg|png|gif)$/", $types[$i])) {
            $value = explode(".", $names[$i]);
            $extension = strtolower(array_pop($value));

            $new_name = md5(date("d/m/y-H:i:s") . "_" . $i) . "." . $extension;
            $upload = move_uploaded_file($tmp_names[$i], $dir . $new_name);

            $imgrs = new imagemanager($dir . $new_name);
            $resize = $imgrs->best_fit(1200, 800)->save($dir . "resize/" . $new_name, 90);

            $imgr = new imagemanager($dir . "resize/" . $new_name);
            $thumb = $imgr->thumbnail(500, 350)->save($dir . "thumb/" . $new_name, 95);

            unlink($dir . $new_name);

            if ($upload and $resize and $thumb) {
                $sql .= "('" . $content->codificar($new_name) . "', $id)";
                if ($j < count($tmp_names)) {
                    $sql .= ", ";
                }
            } else {
                echo "error";
                exit;
            }
        } else {
            echo "error";
            exit;
        }
        $j++;
    }

    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}


// ALTER
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    require "../inc/imagemanager.php";
    @$titulo = $_POST["titulo"];
    @$tipo = $_POST["tipo"];
    @$data = $_POST["data"];
    @$resumo = $_POST["resumo"];
    @$ativo = $_POST["ativo"];
    @$ordem = $_POST["ordem"];
    @$texto = $_POST["texto"];
    @$keywords = $_POST["keywords"];
    @$imagem = $_FILES["imagem"];

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"título\" é obrigatório.";
        exit;
    }

    $datas = explode("/", $data);
    if (!checkdate($datas[1], $datas[0], $datas[2])) {
        echo "A \"data\" informada é inválida.";
        exit;
    }

    if (empty($data)) {
        echo "Campo \"data\" é obrigatório.";
        exit;
    }

    if (empty($purifier->purify($texto))) {
        echo "Campo \"texto\" é obrigatório.";
        exit;
    }

    if (empty($purifier->purify($resumo))) {
        echo "Campo \"resumo\" é obrigatório.";
        exit;
    }

    $data = $content->dataToDate($data);
    $w = date('D', strtotime($data));

    switch ($w) {
        case "Mon":
            $dw = "Segunda-feira";
            break;
        case "Tue":
            $dw = "Terça-feira";
            break;
        case "Wed":
            $dw = "Quarta-feira";
            break;
        case "Thu":
            $dw = "Quinta-feira";
            break;
        case "Fri":
            $dw = "Sexta-feira";
            break;
        case "Sat":
            $dw = "Sábado";
            break;
        case "Sun":
            $dw = "Domingo";
            break;
    }

    if ($content->checkImg($imagem)) {
        $move = $content->upload($imagem, "../../img/upload/");

        $imgr = new imagemanager("../../img/upload/" . $move);
        $thumb = $imgr->thumbnail(1200, 649)->save("../../img/upload/thumb/" . $move, 95);
        unset($imgr);

        unlink("../../img/upload/" . $move);

        if ($move and $thumb) {
            $sql = "UPDATE noticias SET titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', tipo = '" . $content->codificar($content->limpaInsert($tipo)) . "', imagem = '" . $content->codificar($move) . "', data = '" . $content->limpaInsert($data) . "', dia = '" . $content->codificar($content->limpaInsert($dw)) . "', texto = '" . $purifier->purify($texto) . "', resumo = '" . $purifier->purify($resumo) . "', ativo = '" . $content->limpaInsert($ativo) . "', ordem = '" . $content->limpaInsert($ordem) . "', keywords = '" . $content->limpaInsert($keywords) . "' WHERE id = " . $content->limpaInsert($id) ;
            $resultado = $content->sql($sql);
            if ($resultado) {
                echo "done";
                exit;
            } else {
                echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
                exit;
            }
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    }else{
        $sql = "UPDATE noticias SET titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', tipo = '" . $content->codificar($content->limpaInsert($tipo)) . "', data = '" . $content->limpaInsert($data) . "', dia = '" . $content->codificar($content->limpaInsert($dw)) . "', texto = '" . $purifier->purify($texto) . "', resumo = '" . $purifier->purify($resumo) . "', ativo = '" . $content->limpaInsert($ativo) . "', ordem = '" . $content->limpaInsert($ordem) . "', keywords = '" . $content->limpaInsert($keywords) . "' WHERE id = " . $content->limpaInsert($id) ;
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    }
}


// DEL
if (isset($_GET["action"]) and $_GET["action"] === "del") {
    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "DELETE FROM noticias WHERE id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}

//IMAGENS
if (isset($_GET["action"]) and $_GET["action"] === "img_del") {
    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    if (empty($_POST["post"]) or ! ctype_digit((string) $_POST["post"])) {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    } else {
        $post = (int) $_POST["post"];
    }

    @$arquivo = $_POST["imagem"];

    $sql = "DELETE FROM imagens WHERE id = " . $id;
    if ($content->sql($sql)) {
        if (isset($arquivo) and file_exists("../../img/upload/resize/" . $arquivo)) {
            unlink("../../img/upload/resize/" . $arquivo);
        }
        if (isset($arquivo) and file_exists("../../img/upload/thumb/" . $arquivo)) {
            unlink("../../img/upload/thumb/" . $arquivo);
        }
    } else {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    }

    $sql_fotos = "SELECT id, imagem FROM imagens WHERE noticia = $post";
    $resultado_fotos = $content->sql($sql_fotos);
    if ($resultado_fotos) {
        $num_rows_fotos = $content->num_rows($resultado_fotos);
        if ($num_rows_fotos > 0) {
            while ($row_fotos = $content->fetch($resultado_fotos)) {
                ?>
                <div class="alter_img">
                    <a href='javascript:void(0)' class='alter_close' onclick='window.del_img(<?php echo $row_fotos['id'] . ", " . $post . ", \"" . $content->limpaEcho($content->decodificar($row_fotos['imagem'])) . "\""; ?>);'>
                        <img src='img/no.png'/>
                    </a>
                    <div>
                        <a href="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="gallery-item" data-title="" title="clique para ampliar" class="gallery-item">
                            <img src="../img/upload/thumb/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="shadow_2"/>
                        </a>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "Nenhuma imagem cadastrada.";
        }
    } else {
        echo "Nenhuma imagem encontrada.";
    }
    ?>
    <div class="clearfix"></div><br>
    <script type="text/javascript">

        $('.gallery-item').magnificPopup({
            tClose: 'Fechar',
            tLoading: '',
            type: 'image',
            mainClass: 'mfp-zoom-in',
            removalDelay: 200,
            closeBtnInside: false,
            image: {
                titleSrc: 'data-title',
                tError: 'A <a href="%url%">imagem</a> não pode ser carregada.'
            },
            gallery: {
                enabled: true,
                tPrev: 'Anterior',
                tNext: 'Próxima',
                tCounter: ''
            },
            callbacks: {
                imageLoadComplete: function () {
                    var self = this;
                    setTimeout(function () {
                        self.wrap.addClass('mfp-image-loaded');
                    }, 16);
                },
                beforeClose: function () {
                    $(".mfp-arrow").hide();
                },
                close: function () {
                    this.wrap.removeClass('mfp-image-loaded');
                }
            }
        });

    </script>
    <?php
}