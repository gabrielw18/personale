<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }

    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}

//BANNER
if (isset($_GET["action"]) and $_GET["action"] === "novo") {
    require "../inc/imagemanager.php";

    @$titulo = $_POST["titulo"];
    @$subtitulo = $_POST["subtitulo"];
    @$link = $_POST["link"];
    @$ativo = $_POST["ativo"];
    @$imagem = $_FILES["imagem"];
    @$ordem = (isset($_POST["ordem"]) and ctype_digit((string) $_POST["ordem"])) ? $_POST["ordem"] : 0;

    if (!$content->checkImg($imagem)) {
        echo "Selecione uma \"imagem de fundo\" válida.";
        exit;
    }

    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"título\" é obrigatório.";
        exit;
    }

    $move = $content->upload($imagem, "../../img/upload/");

    $imgr = new imagemanager("../../img/upload/" . $move);
    $resize = $imgr->fit_to_width(1920)->mycrop(1920,695)->save("../../img/upload/resize/" . $move, 90);
    unset($imgr);

    unlink("../../img/upload/" . $move);

    if ($move and $resize) {
        $sql = "insert into banners (titulo, subtitulo, link, imagem, ordem, ativo) values ('" . $content->codificar($content->limpaInsert($titulo)) . "', '" . $content->codificar($content->limpaInsert($subtitulo)) . "', '" . $content->codificar($content->limpaInsert($link)) . "', '" . $content->codificar($move) . "', '" . $content->limpaInsert($ordem) . "', '" . $content->limpaInsert($ativo) . "')";
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
            exit;
        }
    } else {
        echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
        exit;
    }
}

//BANNER
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    require "../inc/imagemanager.php";

    @$titulo = $_POST["titulo"];
    @$subtitulo = $_POST["subtitulo"];
    @$link = $_POST["link"];
    @$ativo = $_POST["ativo"];
    @$imagem = $_FILES["imagem"];
    @$ordem = (isset($_POST["ordem"]) and ctype_digit((string) $_POST["ordem"])) ? $_POST["ordem"] : 0;

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde. 1";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"título\" é obrigatório.";
        exit;
    }

    if ($content->checkImg($imagem)) {
        $move = $content->upload($imagem, "../../img/upload/");

        $imgr = new imagemanager("../../img/upload/" . $move);
        $resize = $imgr->fit_to_width(1920)->mycrop(1920,695)->save("../../img/upload/resize/" . $move, 90);
        unset($imgr);

        unlink("../../img/upload/" . $move);

        if ($move and $resize) {
            $sql = "update banners set titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', subtitulo = '" . $content->codificar($content->limpaInsert($subtitulo)) . "', link = '" . $content->codificar($content->limpaInsert($link)) . "', imagem = '" . $content->codificar($move) . "', ordem = " . $content->limpaInsert($ordem) . ", ativo = " . $content->limpaInsert($ativo) . " where id = " . $content->limpaInsert($id);
            $resultado = $content->sql($sql);
            if ($resultado) {
                echo "done";
                exit;
            } else {
                echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
                exit;
            }
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    } else {
        $sql = "update banners set titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', subtitulo = '" . $content->codificar($content->limpaInsert($subtitulo)) . "', link = '" . $content->codificar($content->limpaInsert($link)) . "', ordem = " . $content->limpaInsert($ordem) . ", ativo = " . $content->limpaInsert($ativo) . " where id = " . $content->limpaInsert($id);
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    }
}


//BANNER
if (isset($_GET["action"]) and $_GET["action"] === "del") {
    @$background = $_POST["background"];

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "delete from banners where id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        if (isset($background) and file_exists("../../img/upload/resize/" . $background)) {
            unlink("../../img/upload/resize/" . $background);
        }

        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}