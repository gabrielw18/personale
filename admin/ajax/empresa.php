<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }
    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }
    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}


// NOVO
if (isset($_GET["action"]) and $_GET["action"] === "novo") {
    require "../inc/imagemanager.php";
    @$titulo = $_POST["titulo"];
    @$texto = $_POST["texto"];
    @$telefone = $_POST["telefone"];
    @$telefones = $_POST["telefones"];
    @$email = $_POST["email"];
    @$endereco = $_POST["endereco"];
    @$horario = $_POST["horario"];
    @$ordem = $_POST["ordem"];
    @$mapa = $_POST["mapa"];


    $sql = "INSERT INTO empresas (titulo, texto, telefone, telefones, email, endereco, horario, mapa, ordem) VALUES ('" . $content->codificar($content->limpaInsert($titulo)) . "', '" . $purifier->purify($texto) . "', '" . $content->codificar($content->limpaInsert($telefone)) . "', '" . $content->codificar($content->limpaInsert($telefones)) . "', '" . $content->codificar($content->limpaInsert($email)) . "', '" . $content->codificar($content->limpaInsert($endereco)) . "', '" . $content->codificar($content->limpaInsert($horario)) . "', '" . $purifier->purify($mapa) . "' ,'" . $purifier->purify($ordem) . "')";
    $resultado = $content->sql($sql);

    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
        exit;
    }
}


//ADD IMAGENS A GALERIA
if (isset($_GET["action"]) and $_GET["action"] === "img_novo") {
    require "../inc/imagemanager.php";
    $dir = '../../img/upload/';

    @$imagens = $_FILES['imagens'];

    if (!isset($imagens)) {
        echo "error";
        exit;
    }

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $names = $imagens['name'];
    $tmp_names = $imagens['tmp_name'];
    $types = $imagens['type'];

    $j = 1;
    $sql = "INSERT INTO imagens (imagem, infos) VALUES ";

    for ($i = 0; $i < count($tmp_names); $i++) {
        if (preg_match("/^image\/(jpeg|jpg|png|gif)$/", $types[$i])) {
            $value = explode(".", $names[$i]);
            $extension = strtolower(array_pop($value));

            $new_name = md5(date("d/m/y-H:i:s") . "_" . $i) . "." . $extension;
            $upload = move_uploaded_file($tmp_names[$i], $dir . $new_name);

            $imgrs = new imagemanager($dir . $new_name);
            $resize = $imgrs->best_fit(200, 200)->save($dir . "resize/" . $new_name, 90);

            $imgr = new imagemanager($dir . "resize/" . $new_name);
            $thumb = $imgr->thumbnail(64, 64)->save($dir . "thumb/" . $new_name, 95);

            unlink($dir . $new_name);

            if ($upload and $resize and $thumb) {
                $sql .= "('" . $content->codificar($new_name) . "', $id)";
                if ($j < count($tmp_names)) {
                    $sql .= ", ";
                }
            } else {
                echo "error";
                exit;
            }
        } else {
            echo "error";
            exit;
        }
        $j++;
    }

    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}


// ALTER
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    require "../inc/imagemanager.php";
    @$titulo = $_POST["titulo"];
    @$texto = $_POST["texto"];
    @$telefone = $_POST["telefone"];
    @$telefones = $_POST["telefones"];
    @$email = $_POST["email"];
    @$endereco = $_POST["endereco"];
    @$horario = $_POST["horario"];
    @$ordem = $_POST["ordem"];
    @$mapa = $_POST["mapa"];

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }


    $sql = "UPDATE empresas SET titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', texto = '" . $purifier->purify($texto) . "', telefone = '" . $content->codificar($content->limpaInsert($telefone)) . "', telefones = '" . $content->codificar($content->limpaInsert($telefones)) . "', email = '" . $content->codificar($content->limpaInsert($email)) . "', endereco = '" . $content->codificar($content->limpaInsert($endereco)) . "', horario = '" . $content->codificar($content->limpaInsert($horario)) . "', mapa = '" . $purifier->purify($mapa) . "', ordem = '" . $content->limpaInsert($ordem) . "' WHERE id = " . $content->limpaInsert($id) ;
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    }
}


// DEL
if (isset($_GET["action"]) and $_GET["action"] === "del") {
    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "DELETE FROM empresas WHERE id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}

//IMAGENS
if (isset($_GET["action"]) and $_GET["action"] === "img_del") {
    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    if (empty($_POST["post"]) or ! ctype_digit((string) $_POST["post"])) {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    } else {
        $post = (int) $_POST["post"];
    }

    @$arquivo = $_POST["imagem"];

    $sql = "DELETE FROM imagens WHERE id = " . $id;
    if ($content->sql($sql)) {
        if (isset($arquivo) and file_exists("../../img/upload/resize/" . $arquivo)) {
            unlink("../../img/upload/resize/" . $arquivo);
        }
        if (isset($arquivo) and file_exists("../../img/upload/thumb/" . $arquivo)) {
            unlink("../../img/upload/thumb/" . $arquivo);
        }
    } else {
        echo "Erro ao excluir imagem. Tente novamente mais tarde.";
        exit;
    }

    $sql_fotos = "SELECT id, imagem FROM imagens WHERE infos = $post";
    $resultado_fotos = $content->sql($sql_fotos);
    if ($resultado_fotos) {
        $num_rows_fotos = $content->num_rows($resultado_fotos);
        if ($num_rows_fotos > 0) {
            while ($row_fotos = $content->fetch($resultado_fotos)) {
                ?>
                <div class="alter_img">
                    <a href='javascript:void(0)' class='alter_close' onclick='window.del_img(<?php echo $row_fotos['id'] . ", " . $post . ", \"" . $content->limpaEcho($content->decodificar($row_fotos['imagem'])) . "\""; ?>);'>
                        <img src='../img/admin/no.png'/>
                    </a>
                    <div>
                        <a href="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="gallery-item" data-title="" title="clique para ampliar" class="gallery-item">
                            <img src="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="shadow_2"/>
                        </a>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "Nenhuma imagem cadastrada.";
        }
    } else {
        echo "Nenhuma imagem encontrada.";
    }
    ?>
    <div class="clearfix"></div><br>
    <script type="text/javascript">

        $('.gallery-item').magnificPopup({
            tClose: 'Fechar',
            tLoading: '',
            type: 'image',
            mainClass: 'mfp-zoom-in',
            removalDelay: 200,
            closeBtnInside: false,
            image: {
                titleSrc: 'data-title',
                tError: 'A <a href="%url%">imagem</a> não pode ser carregada.'
            },
            gallery: {
                enabled: true,
                tPrev: 'Anterior',
                tNext: 'Próxima',
                tCounter: ''
            },
            callbacks: {
                imageLoadComplete: function () {
                    var self = this;
                    setTimeout(function () {
                        self.wrap.addClass('mfp-image-loaded');
                    }, 16);
                },
                beforeClose: function () {
                    $(".mfp-arrow").hide();
                },
                close: function () {
                    this.wrap.removeClass('mfp-image-loaded');
                }
            }
        });

    </script>
    <?php
}