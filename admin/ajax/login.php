<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

//LOGIN
if (isset($_GET["action"]) and $_GET["action"] === "login") {
    @$email = $_POST["email"];
    @$password = $_POST["senha"];

    if (empty($email) or ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Preencha o campo \"e-mail\" corretamente";
        exit;
    }

    if (empty($password)) {
        echo "Preencha o campo \"senha\" corretamente";
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($email) . "' and senha = '" . code1 . sha1($password) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows === 1) {
            while ($row = $content->fetch($resultado)) {
                $dados = array();
                $dados["email"] = $row['email'];
                $dados["senha"] = $content->codificar($password);
                $_SESSION["dados" . project] = $dados;

                echo "access-granted";
                exit;
            }
        } else {
            echo "Dados incorretos. Revise e tente novamente.";
            exit;
        }
    } else {
        echo "Dados incorretos. Revise e tente novamente.";
        exit;
    }
}