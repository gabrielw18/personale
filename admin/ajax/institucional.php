<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }

    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}

//INSTITUCIONAL
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    require "../inc/imagemanager.php";

    @$texto1 = $_POST['texto1'];
    @$resumo = $purifier->purify($_POST['resumo']);

    @$imagem = $_FILES["imagem"];

    if ($content->checkImg($imagem)) {
        $lead = $content->upload($imagem, "../../img/upload/");

        $imgr_2 = new imagemanager("../../img/upload/" . $lead);
        $leader = $imgr_2->fit_to_width(400)->save("../../img/upload/thumb/" . $lead, 100);
        unset($imgr_2);

        unlink("../../img/upload/" . $lead);

        if ($lead and $leader) {
            $sql = "UPDATE institucional SET texto = '" . $purifier->purify($texto) . "', resumo = '" . $purifier->purify($resumo) . "',  imagem = '" . $content->codificar($lead) . "' WHERE id = 1";
            $resultado = $content->sql($sql);
            if ($resultado) {
                echo "done";
                exit;
            } else {
                echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
                exit;
            }
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    } else {
        $sql = "UPDATE institucional SET texto1 = '" . $purifier->purify($texto1) . "', resumo = '" . $purifier->purify($resumo) . "' WHERE id = 1";
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    }
}