<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//LOGIN
if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        echo "reload";
        exit;
    }

    if (empty($content->decodificar($dados["senha"]))) {
        echo "reload";
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows !== 1) {
            echo "reload";
            exit;
        }
    } else {
        echo "reload";
        exit;
    }
} else {
    echo "reload";
    exit;
}


// Metodologia
if (isset($_GET["action"]) and $_GET["action"] === "novo") {
    require "../inc/imagemanager.php";

    @$imagem = $_FILES["imagem"];
    @$titulo = $_POST["titulo"];
    @$empresa = $_POST["empresa"];
    @$texto = $_POST["texto"];
    @$ordem = (isset($_POST["ordem"]) and ctype_digit((string) $_POST["ordem"])) ? $_POST["ordem"] : 0;

    if (!$content->checkImg($imagem)) {
        echo "Selecione uma \"imagem\" válida.";
        exit;
    }

    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"titulo\" é obrigatório.";
        exit;
    }

    $move = $content->upload($imagem, "../../img/upload/");

    $imgr = new imagemanager("../../img/upload/" . $move);
    $resize = $imgr->fit_to_width(1000)->mycrop(1000, 600)->save("../../img/upload/resize/" . $move, 100);
    unset($imgr);


    unlink("../../img/upload/" . $move);

    if ($move and $resize) {
        $sql = "INSERT INTO fotosempresa (imagem, titulo, empresa, texto, ordem) VALUES ('" . $content->codificar($move) . "', '" . $content->codificar($content->limpaInsert($titulo)) . "', '" . $content->codificar($content->limpaInsert($empresa)) . "', '" . $purifier->purify($texto) . "', " . $content->limpaInsert($ordem) . ")";
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
            exit;
        }
    } else {
        echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
        exit;
    }
}


// METODOLOGIA
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    require "../inc/imagemanager.php";

    @$imagem = $_FILES["imagem"];
    @$titulo = $_POST["titulo"];
    @$empresa = $_POST["empresa"];
    @$texto = $_POST["texto"];
    @$ordem = (isset($_POST["ordem"]) and ctype_digit((string) $_POST["ordem"])) ? $_POST["ordem"] : 0;

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    if (empty($content->limpaInsert($titulo))) {
        echo "Campo \"título\" é obrigatório.";
        exit;
    }

    if ($content->checkImg($imagem)) {
        $move = $content->upload($imagem, "../../img/upload/");

        $imgr = new imagemanager("../../img/upload/" . $move);
        $resize = $imgr->fit_to_width(1000)->mycrop(1000, 600)->save("../../img/upload/resize/" . $move, 100);
        unset($imgr);


        unlink("../../img/upload/" . $move);

        if ($move and $resize) {
            $sql = "UPDATE fotosempresa SET imagem = '" . $content->codificar($move) . "', titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', empresa = '" . $content->codificar($content->limpaInsert($empresa)) . "', texto = '" . $purifier->purify($texto) . "', ordem = " . $content->limpaInsert($ordem) . " WHERE id = " . $content->limpaInsert($id);
            $resultado = $content->sql($sql);
            if ($resultado) {
                echo "done";
                exit;
            } else {
                echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
                exit;
            }
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    } else {
        $sql = "UPDATE fotosempresa SET titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', empresa = '" . $content->codificar($content->limpaInsert($empresa)) . "', texto = '" . $purifier->purify($texto) . "', ordem = " . $content->limpaInsert($ordem) . " WHERE id = " . $content->limpaInsert($id);
        $resultado = $content->sql($sql);
        if ($resultado) {
            echo "done";
            exit;
        } else {
            echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
            exit;
        }
    }
}

//METODOLOGIA
if (isset($_GET["action"]) and $_GET["action"] === "del") {
    @$arquivo = $_POST["arquivo"];

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "DELETE FROM fotoempresa WHERE id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        if (isset($arquivo) and file_exists("../../img/upload/resize/" . $arquivo)) {
            unlink("../../img/upload/resize/" . $arquivo);
        }
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}
