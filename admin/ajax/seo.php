<?php

session_start();
session_regenerate_id();
require "../inc/init.php";
require "../inc/connect.php";
$content = new content();

require '../HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

//NOVO SEO
if (isset($_GET["action"]) and $_GET["action"] === "novo") {
    @$pagina = $_POST["pagina"];
    @$titulo = $_POST["titulo"];
    @$keywords = $_POST["keywords"];
    @$description = $_POST["description"];


    $sql = "INSERT INTO seo (pagina, titulo, keywords, description) VALUES ('" . $content->limpaInsert($pagina) . "', '" . $content->codificar($content->limpaInsert($titulo)) . "', '" . $content->codificar($content->limpaInsert($keywords)) . "', '" . $content->codificar($content->limpaInsert($description)) . "')";
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao cadastrar. Tente novamente mais tarde.";
        exit;
    }
}

//ALTER SEO
if (isset($_GET["action"]) and $_GET["action"] === "alter") {
    @$pagina = $_POST["pagina"];
    @$titulo = $_POST["titulo"];
    @$keywords = $_POST["keywords"];
    @$description = $_POST["description"];

    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "UPDATE seo SET pagina = '" . $content->limpaInsert($pagina) . "', titulo = '" . $content->codificar($content->limpaInsert($titulo)) . "', keywords = '" . $content->codificar($content->limpaInsert($keywords)) . "', description = '" . $content->codificar($content->limpaInsert($description)) . "' WHERE id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "Erro inesperado ao atualizar. Tente novamente mais tarde.";
        exit;
    }
}

//DEL SEO
if (isset($_GET["action"]) and $_GET["action"] === "del") {
    if (empty($_POST["id"]) or ! ctype_digit((string) $_POST["id"])) {
        echo "error";
        exit;
    } else {
        $id = (int) $_POST["id"];
    }

    $sql = "DELETE FROM seo WHERE id = " . $content->limpaInsert($id);
    $resultado = $content->sql($sql);
    if ($resultado) {
        echo "done";
        exit;
    } else {
        echo "error";
        exit;
    }
}