<?php
if (!isset($_SESSION)) {
  session_start();
}
session_regenerate_id();
require "init.php";
require "connect.php";
$content = new content();

require 'HTMLPurifier/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Trusted', true);
$config->set('HTML.SafeIframe', true);
$config->set('HTML.ForbiddenElements', array('script', 'applet'));
$config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.google\.com/maps/)%');
$purifier = new HTMLPurifier($config);

if (isset($_SESSION["dados" . project])) {
    $dados = $_SESSION["dados" . project];

    if (empty($dados["email"]) or ! filter_var($dados["email"], FILTER_VALIDATE_EMAIL)) {
        header("Location: inc/logout.php");
        exit;
    }

    if (empty($content->decodificar($dados["senha"]))) {
        header("Location: inc/logout.php");
        exit;
    }

    $sql = "select email from usuarios where email = '" . $content->limpaInsert($dados["email"]) . "' and senha = '" . code1 . sha1($content->decodificar($dados["senha"])) . code2 . "'";
    $resultado = $content->sql($sql);
    if ($resultado) {
        $num_rows = $content->num_rows($resultado);
        if ($num_rows === 1) {
            while ($row = $content->fetch($resultado)) {
                $dados = array();
                $dados["email"] = $row['email'];
                $dados["senha"] = $_SESSION["dados" . project]["senha"];
                $_SESSION["dados" . project] = $dados;
            }
        } else {
            header("Location: inc/logout.php");
            exit;
        }
    } else {
        header("Location: inc/logout.php");
        exit;
    }
} else {
    header("Location: inc/logout.php");
    exit;
}