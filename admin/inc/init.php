<?php

//error_reporting(0);

ini_set('session.cookie_httponly', 1);
if (basename($_SERVER["PHP_SELF"]) == "init.php") {
    die();
}

if (get_magic_quotes_runtime()) {
    set_magic_quotes_runtime(0);
}

function remove_mq(&$var) {
    return is_array($var) ? array_map("remove_mq", $var) : stripslashes($var);
}

if (get_magic_quotes_gpc()) {
    $_GET = array_map("remove_mq", $_GET);
    $_POST = array_map("remove_mq", $_POST);
    $_COOKIE = array_map("remove_mq", $_COOKIE);
    $_SESSION = array_map("remove_mq", $_SESSION);
}

if (function_exists("ini_get")) {
    if (!ini_get("display_errors")) {
        ini_set("display_errors", 1);
    }
    if (ini_get("magic_quotes_sybase")) {
        ini_set("magic_quotes_sybase", 0);
    }
}
clearstatcache();