<?php

date_default_timezone_set("America/Sao_Paulo");
// define("path", "http://" . $_SERVER['HTTP_HOST']);
define("path", "http://" . $_SERVER['HTTP_HOST']. '/personale');
define("project", "Kahle e Bitencourt Advogados");
define("code1", "a38a60essf830ybfe89deasa43");
define("code2", "274a3d186b0637837d5d741e");

class content {

    // private $host = "localhost";
    // private $user = "root";
    // private $pass = "";
    // private $banco = "db_personale";

    // p3rs0n4LE

    private $host = "mysql642.umbler.com:41890";
    private $user = "personale";
    private $pass = "p3rs0n4LE";
    private $banco = "personale";

    private $connect;

    function __construct() {
        $this->connect = mysqli_connect($this->host, $this->user, $this->pass, $this->banco);
    }

    function __destruct() {
        mysqli_close($this->connect);
    }

    function sql($sql) {
        mysqli_query($this->connect, "SET NAMES utf8");
        $query = mysqli_query($this->connect, $sql);
        return $query;
    }

    function num_rows($resultado) {
        $num_rows = mysqli_num_rows($resultado);
        return $num_rows;
    }

    function fetch($resultado) {
        $row = mysqli_fetch_array($resultado);
        return $row;
    }

    function limpaInsert($string) {
        $var = trim($string);
        $var = strip_tags($var);

        $array = array("\r\n", "\n\r", "\n", "\r");
        $var = str_replace($array, "break-line-replacement", $var);

        return mysqli_real_escape_string($this->connect, $var);
    }

    function limpaEcho($string) {
        $var = trim($string);
        $var = strip_tags($var);
        return str_replace("break-line-replacement", "<br>", $var);
    }

    function codificar($str) {
        $prfx = array('AFVxaIF', 'Vzc2ddS', 'ZEca3d1', 'aOdhlVq', 'QhdFmVJ', 'VTUaU5U',
            'QRVMuiZ', 'lRZnhnU', 'Hi10dX1', 'GbT9nUV', 'TPnZGZz', 'ZGiZnZG',
            'dodHJe5', 'dGcl0NT', 'Y0NeTZy', 'dGhnlNj', 'azc5lOD', 'BqbWedo',
            'bFmR0Mz', 'Q1MFjNy', 'ZmFMkdm', 'dkaDIF1', 'hrMaTk3', 'aGVFsbG');
        for ($i = 0; $i < 3; $i++) {
            $str = $prfx[array_rand($prfx)] . strrev(base64_encode($str));
        }
        $str = strtr($str, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "aQbrnBcTw50pZfz7iUJAqt6kuLjxe38Pl9MgV4OKYsEyFGDvhmNWXodSH1RIC2");
        return $str;
    }

    function decodificar($str) {
        $str = strtr($str, "aQbrnBcTw50pZfz7iUJAqt6kuLjxe38Pl9MgV4OKYsEyFGDvhmNWXodSH1RIC2", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        for ($i = 0; $i < 3; $i++) {
            $str = base64_decode(strrev(substr($str, 7)));
        }
        return strip_tags(stripslashes($str));
    }

    function checkImg($imagem) {
        if (!empty($imagem['tmp_name']) and preg_match("/^image\/(jpeg|jpg|png|gif|bmp|JPEG|JPG|PNG|GIF|BMP)$/", $imagem['type'])) {
            return true;
        } else {
            return false;
        }
    }

    function cortaFrase($frase, $qtde_letras) {
        $frase = str_replace('&nbsp;', ' ', $frase);
        $frase = strip_tags($frase);
        $p = explode(' ', $frase);
        $c = 0;
        $cortada = '';

        foreach ($p as $p1) {
            if ($c < $qtde_letras && ($c + strlen($p1) <= $qtde_letras)) {
                $cortada .= ' ' . $p1;
                $c += strlen($p1) + 1;
            } else {
                break;
            }
        }
        return strlen($cortada) < strlen($frase) ? html_entity_decode($cortada . '...', ENT_QUOTES, 'UTF-8') : html_entity_decode($cortada, ENT_QUOTES,'UTF-8');
    }

    function get($sql, $error) {
        $resultado = $this->sql($sql);
        if ($resultado) {
            $num_rows = $this->num_rows($resultado);
            if ($num_rows > 0) {
                $row = $this->fetch($resultado);
                echo $row['texto'];
            } else {
                echo $error;
            }
        } else {
            echo $error;
        }
    }

    function tiraAcentos($palavra) {
        $palavra = str_replace(" - ", "-", $palavra);
        $map = array('•' => '', '°' => '', '?' => '', '/' => '', ':' => '', ';' => '', '>' => '', '.' => '', '<' => '', ',' => '', '|' => '', '\\' => '', 'º' => '', '}' => '', ']' => '', '^' => '', '~' => '', 'ª' => '', '{' => '', '[' => '', '`' => '', '´' => '', '§' => '', '=' => '', '+' => '', '_' => '', '-' => '', ')' => '', '(' => '', '*' => '', '&' => '', '¬' => '', '¨' => '', '¢' => '', '%' => '', '£' => '', '$' => '', '³' => '', '#' => '', '²' => '', '@' => '', '¹' => '', '!' => '', "'" => "", '"' => '', ' ' => '-', 'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');
        return strtolower(strtr($palavra, $map));
    }

    function dateToData($data) {
        $data_array = explode("-", $data);
        return $data_array[2] . "/" . $data_array[1] . "/" . $data_array[0];
    }

    function dataToDate($data) {
        $data_array = explode("/", $data);
        return $data_array[2] . "-" . $data_array[1] . "-" . $data_array[0];
    }

    function upload($imagem, $destination) {
        $name = $imagem['name'];
        $tmpName = $imagem['tmp_name'];
        $parts = explode('.', $name);
        $file_extension = array_pop($parts);
        $newName = substr(md5(uniqid(rand(), true)), 3, 23) . "." . $file_extension;
        move_uploaded_file($tmpName, $destination . $newName);
        return $newName;
    }

}
