<?php

session_start();
session_regenerate_id();
require "init.php";
require "connect.php";
$content = new content();

$_SESSION["dados" . project] = null;
unset($_SESSION["dados" . project]);
session_destroy();

header("Location: ../login.php");
exit;