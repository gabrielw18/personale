<!DOCTYPE html>
<html>
    <head>
        <title>Gerenciador de Conteúdo</title>
        <link rel="icon" type="image/png" href="../img/favicon.png">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, follow">

        <!--[if lte IE 8]>
          <script type="text/javascript">
            window.location = "../inc/update.php";
          </script>
        <![endif]-->

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

        <link href='../vendor/bootstrap/css/bootstrap.css' rel="stylesheet" type="text/css"/>
        <link href='../vendor/bootstrap/css/bootstrap-nonresponsive.css' rel="stylesheet" type="text/css"/>
        <link href='../vendor/bootstrap/css/bootstrap-datetimepicker.min.css' rel="stylesheet" type="text/css"/>
        <link href='../vendor/datatables/datatables.min.css' rel="stylesheet" type="text/css"/>
        <link href='../vendor/datatables/plugins/bootstrap/datatables.bootstrap.css' rel="stylesheet" type="text/css"/>
        <link href='css/magnific-popup.css' rel='stylesheet' type="text/css">
        <link href='css/style.css' rel="stylesheet" type="text/css"/>
    </head>
    <body class="body_admin">
        <div class="logo_admin">
            <img src="img/logo.png">
            <div class="log">
                <span>Olá, <?php echo $_SESSION["dados" . project]["email"] ?></span>
                <a href="inc/logout.php" class="btn btn-info">Sair</a>
            </div>
        </div>

        <nav class="navbar navbar-default nav_admin" role="navigation">
            <div class="container-fluid">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="pg-home"><a href="index.php">Home</a></li>
                        <li class="pg-banners"><a href="banners.php">Banners</a></li>
                        <li class="pg-institucional"><a href="institucional.php">Institucional</a></li>
                        <li class="pg-noticias"><a href="noticias.php">Notícias</a></li>
                        <!-- <li class="pg-seo"><a href="seo.php">SEO</a></li> -->
                        <li class="pg-infos"><a href="infos.php">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>