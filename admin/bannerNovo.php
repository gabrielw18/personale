<?php
require 'inc/protect.php';
$thisPage = 'banners';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Novo Banner</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>
                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <form id="form" accept-charset="utf-8">

                    <label for="imagem"><h4>Imagem de Fundo <span class="obs_admin">(dimensão: 1920px x 695px)</span> <span class="error" id="error_2"></span></h4></label>
                    <input type="file" id="imagem" name="imagem"><br>

                    <label for="titulo"><h4>Titulo <span class="error" id="error_1"></span></h4></label>
                    <input type="text" id="titulo" class="form-my-control form-control" name="titulo"><br>

                    <label for="subtitulo"><h4>Subtitulo </h4></label>
                    <input type="text" id="subtitulo" class="form-my-control form-control" name="subtitulo"><br>

                    <label for="link"><h4>Link</h4></label>
                    <input type="text" id="link" class="form-my-control form-control" name="link"><br>

                    <label for="texto"><h4>Ativo</h4></label>
                    <select name="ativo" class="form-my-control form-control" style="width: 100px; min-width: 100px">
                        <option value="0">Inativo</option>
                        <option value="1">Ativo</option>
                    </select><br>

                    <label for="ordem"><h4>Ordem</h4></label>
                    <input type="text" id="ordem" class="form-my-control form-control numeric" name="ordem" style="width: 100px; min-width: 100px"><br>

                    <div class="clearfix"></div><br>

                    <button class="btn btn-success">Salvar</button>

                </form>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">
            $('.pg-<?=$thisPage?>').addClass('active');

            $("#form").submit(function (e) {
                $("#admin_result").html("");
                $(".error").html("");
                var error = 0;

                if ($("#titulo").val() === "") {
                    $("#error_1").html("* campo obrigatório");
                    error++;
                }

                if (!$("#imagem").val().match(/(?:gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/)) {
                    $("#error_2").html("* este arquivo não é uma imagem");
                    error++;
                }

                if ($("#imagem").val() === "") {
                    $("#error_2").html("* campo obrigatório");
                    error++;
                }

                if (error === 0) {
                    var form_data = new FormData($(this)[0]);

                    $.ajax({
                        url: 'ajax/banner.php?action=novo',
                        data: form_data,
                        type: "POST",
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                            $.smoothScroll({
                                scrollTarget: '#admin_result',
                                offset: -20,
                                speed: 200
                            });
                        },
                        success: function (result) {
                            switch (result) {
                                case 'reload':
                                    window.location = "inc/logout.php";
                                    break;
                                case 'done':
                                    window.location = "banners.php?status=success";
                                    break;
                                default:
                                    $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                    break;
                            }
                        }
                    });
                }

                e.preventDefault();
                // $(this).unbind(e);
            });

        </script>
    </body>
</html>