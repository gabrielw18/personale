<?php
require 'inc/protect.php';

$thisPage = 'seo';

?>

<?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">SEO</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <?php
                if (isset($_GET['status'])) {
                    if ($_GET['status'] == 'success') {
                        ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Ação efetuada com sucesso!
                        </div>
                        <?php
                    } elseif ($_GET['status'] == 'error') {
                        ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Erro ao efetuar ação. Tente novamente mais tarde.
                        </div>
                        <?php
                    }
                }
                ?>


                <?php
                $sql = "SELECT id, pagina, titulo, keywords, description FROM seo ORDER BY id";
                $resultado = $content->sql($sql);
                if ($resultado) {
                    $num_rows = $content->num_rows($resultado);
                    if ($num_rows > 0) {
                        ?>
                        <table class="table_admin">
                            <thead>
                                <tr>
                                    <td width="120px">Pagina</td>
                                    <td width="160px">Título</td>
                                    <td>Palavras Chaves</td>
                                    <td>Descrição</td>
                                    <td>Ações</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($row = $content->fetch($resultado)) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $content->limpaEcho(($row['pagina'])) ?></td>
                                        <td class="text-center no-break"><?php echo $content->limpaEcho($content->decodificar($row['titulo'])) ?></td>
                                        <td><?php echo $content->limpaEcho($content->decodificar($row['keywords'])) ?></td>
                                        <td><?php echo $content->limpaEcho($content->decodificar($row['description'])) ?></td>
                                        <td class="td_icon"><a href="seoalter.php?id=<?php echo $row['id'] ?>" title="editar"><img src="../img/admin/edit.png"></a></td>
                                        <!-- <td class="td_icon"><a class="del" data-id="<?php echo $row['id'] ?>" data-arquivo="<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" title="excluir"><img src="../img/admin/no.png"></a></td> -->
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        echo "<div class='text-center'>Nenhum cadastro encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Nenhum cadastro encontrado.</div>";
                }
                ?>
            </div>
        </div>

        <div id="modal_confirm" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-admin">
                        <h4 class="modal-title title-admin">Confirmação</h4>
                    </div>
                    <div class="modal-body body-admin">
                        <p>Você tem certeza que deseja excluir isto?<br>Essa ação não pode ser desfeita.</p>
                        <input type="hidden" id="confirm-id" value="">
                        <input type="hidden" id="confirm-imagem" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        <button id="del" type="button" class="btn btn-danger" style="margin-left: 5px">Excluir</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/color.js"></script>
        <script type="text/javascript" src="../js/easing.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">

            $('.pg-<?=$thisPage?>').addClass('active');

            $(".del").click(function () {
                var id = $(this).attr("data-id");
                var arquivo = $(this).attr("data-arquivo");

                $("#confirm-id").val(id);
                $("#confirm-imagem").val(arquivo);
                $("#modal_confirm").modal();
            });

            $("#del").click(function () {
                var form_data = {
                    id: $('#confirm-id').val(),
                    arquivo: $('#confirm-imagem').val()
                };

                $.ajax({
                    url: 'ajax/seo.php?action=del',
                    data: form_data,
                    type: "POST",
                    success: function (result) {
                        switch (result) {
                            case 'reload':
                                window.location = "inc/logout.php";
                                break;
                            case 'done':
                                window.location = "seo.php?status=success";
                                break;
                            default:
                                window.location = "seo.php?status=error";
                                break;
                        }
                    }
                });
            });

        </script>
    </body>
</html>