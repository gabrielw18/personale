<?php
require 'inc/protect.php';
$thisPage = 'noticias';
?>

    <?php include 'inc/topo.php'; ?>
        <div class="wrap_admin">
            <h3 class="header_admin">Nova notícia</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>

                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <form id="form" accept-charset="utf-8">

                    <label for="titulo"><h4>Título <span class="error" id="error_2"></span></h4></label>
                    <input type="text" id="titulo" class="form-my-control form-control" name="titulo"><br>

                    <label for="data"><h4>Data <span class="error" id="error_3"></span></h4></label>
                    <input type="text" id="data" class="form-my-control form-control" name="data" style="width: 200px; min-width: 200px"><br>

                    <label for="imagem"><h4>Imagem de capa <span class="obs_admin">(dimensões exatas: largura 1200px - altura 649px)</span> <span class="error" id="error_1"></span></h4></label>
                    <input type="file" id="imagem" name="imagem"><br>

                    <label for="resumo"><h4>Resumo <span class="error"></span></h4></label>
                    <textarea id="resumo" name="resumo" class="form-my-control form-control"></textarea><br>

                    <label for="keywords"><h4>Palavras chaves <span class="error"></span></h4></label>
                    <textarea id="keywords" name="keywords" class="form-my-control form-control"></textarea><br>

                    <label for="texto"><h4>Texto <span class="error" id="error_4"></span></h4></label>
                    <textarea id="texto" class="tinymce form-my-control form-control"></textarea><br>

                    <label for="ativo"><h4>Ativo</h4></label>
                    <select id="ativo" name="ativo" class="form-my-control form-control" style="width: 100px; min-width: 100px">
                        <option value="0" selected="selected">Inativo</option>
                        <option value="1">Ativo</option>
                    </select><br>

                    <label for="ordem"><h4>Ordem</h4></label>
                    <input type="text" id="ordem" class="form-my-control form-control numeric" name="ordem" style="width: 100px; min-width: 100px"><br>

                    <div class="clearfix"></div><br>

                    <button class="btn btn-success">Salvar</button>

                </form>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/moment-with-locales.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/jquery.mask.min.js"></script>
        <script type="text/javascript" src="TinyMCE/tinymce.min.js"></script>
        <script type="text/javascript">

                    $('.pg-<?=$thisPage?>').addClass('active');

                    $("#form").submit(function (e) {
                        $("input").blur();
                        $("textarea").blur();
                        $("#admin_result").html("");
                        $(".error").html("");
                        var error = 0;

                        if (!$("#imagem").val().match(/(?:gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/)) {
                            $("#error_1").html("* este arquivo não é uma imagem");
                            error++;
                        }

                        if ($("#imagem").val() === "") {
                            $("#error_1").html("* campo obrigatório");
                            error++;
                        }

                        if ($("#titulo").val() === "") {
                            $("#error_2").html("* campo obrigatório");
                            error++;
                        }

                        var data = moment($("#data").val(), "DD/MM/YYYY");
                        if (!data.isValid()) {
                            $("#error_3").html("* campo obrigatório");
                            error++;
                        }

                        var texto = tinyMCE.get('texto').getContent();
                        if (texto === "") {
                            $("#error_4").html("* campo obrigatório");
                            error++;
                        }

                        if (error === 0) {
                            var form_data = new FormData($(this)[0]);
                            form_data.append('texto', texto);

                            $.ajax({
                                url: 'ajax/noticia.php?action=novo',
                                data: form_data,
                                type: "POST",
                                cache: false,
                                contentType: false,
                                processData: false,
                                beforeSend: function () {
                                    $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                                    $.smoothScroll({
                                        scrollTarget: '#admin_result',
                                        offset: -20,
                                        speed: 200
                                    });
                                },
                                success: function (result) {
                                    switch (result) {
                                        case 'reload':
                                            window.location = "inc/logout.php";
                                            break;
                                        case 'done':
                                            window.location = "noticias.php?status=success";
                                            break;
                                        default:
                                            $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                            break;
                                    }
                                }
                            });
                        }

                        e.preventDefault();
                    });

                    tinymce.init({
                        selector: '.tinymce',
                        autoresize_min_height: 0,
                        language: "pt_BR",
                        theme: 'modern',
                        menu: {},
                        plugins: [
                            'autoresize advlist autolink lists link image charmap hr anchor',
                            'searchreplace code fullscreen',
                            'media save table contextmenu directionality',
                            'paste textcolor colorpicker textpattern imagetools jbimages'
                        ],
                        toolbar1: 'code | undo redo | styleselect | bold italic underline strikethrough removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink | jbimages media | table',
                        image_advtab: true,
                        imagetools_toolbar: "imageoptions",
                        relative_urls: false
                    });

                    $(function () {
                        $('#data').datetimepicker({
                            language: 'pt-BR',
                            pickTime: false,
                            format:'DD/MM/YYYY'
                        });
                    });

                    $(document).ready(function () {
                        $('#data').mask('00/00/0000', {clearIfNotMatch: true});
                    });

        </script>
    </body>
</html>