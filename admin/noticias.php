<?php
require 'inc/protect.php';
$thisPage = 'noticias';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Notícias</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <?php
                if (isset($_GET['status'])) {
                    if ($_GET['status'] == 'success') {
                        ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Ação efetuada com sucesso!
                        </div>
                        <?php
                    } elseif ($_GET['status'] == 'error') {
                        ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Erro ao efetuar ação. Tente novamente mais tarde.
                        </div>
                        <?php
                    }
                }
                ?>
                <!-- <a href="portfolio.php" class="btn btn-info active">Lista de Projetos</a>
                <a href="portfolio-cat.php" class="btn btn-info">Categorias Portfólio</a><br><br> -->
                <a href="noticiaNovo.php" class="btn btn-success">Nova notícia</a><br><br>

                <?php
                $sql = "SELECT id, data, titulo, tipo, imagem, texto, resumo, ativo FROM noticias ORDER BY ordem, id DESC";
                $resultado = $content->sql($sql);
                if ($resultado) {
                    $num_rows = $content->num_rows($resultado);
                    if ($num_rows > 0) {
                        ?>
                        <table class="table_admin">
                            <thead>
                                <tr>
                                    <td>Capa</td>
                                    <td width="140px">Data</td>
                                    <td>Título</td>
                                    <td>Resumo</td>
                                    <td colspan="3">Ações</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($row = $content->fetch($resultado)) {
                                    ?>
                                    <tr>
                                        <td class="text-center" style="width: 110px; padding: 15px"><img src="../img/upload/thumb/<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" style="width: 110px; padding: 0px; background-color: #fff"></td>
                                        <td class="text-center"><?php echo $content->dateToData($row['data']) ?></td>
                                        <td class="text-center"><?php echo $content->limpaEcho($content->decodificar($row['titulo'])) ?></td>
                                        <td><?php echo $purifier->purify($row['resumo']) ?></td>
                                        <td class="td_icon"><a class="add_images" data-id="<?php echo $row['id'] ?>" title="adicionar imagem(ns)"><img src="img/img.png"></a></td>
                                        <td class="td_icon"><a href="noticiaAlter.php?id=<?php echo $row['id'] ?>" title="editar"><img src="img/edit.png"></a></td>
                                        <td class="td_icon"><a class="del" data-id="<?php echo $row['id'] ?>" title="excluir"><img src="img/no.png"></a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        echo "<div class='text-center'>Nenhum cadastro encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Nenhum cadastro encontrado.</div>";
                }
                ?>

            </div>
        </div>

        <div id="modal_confirm" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-admin">
                        <h4 class="modal-title title-admin">Confirmação</h4>
                    </div>
                    <div class="modal-body body-admin">
                        <p>Você tem certeza que deseja excluir isto?<br>Essa ação não pode ser desfeita.</p>
                        <input type="hidden" id="confirm-id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        <button id="del" type="button" class="btn btn-danger" style="margin-left: 5px">Excluir</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_images" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-admin">
                        <h4 class="modal-title title-admin">Adicionar imagem(ns): 1200x649px</h4>
                    </div>
                    <div class="modal-body body-admin">
                        <div id="loading-images"></div>
                        <p>Selecione a(s) imagem(ns) para adicionar a esta notícia.<br>(no máximo, 10 imagens por vez)</p>

                        <form id="form_images" style="padding: 15px">
                            <input type="file" name="imagens[]" id="imagens" multiple="" autocomplete="off">
                            <input type="hidden" id="images-id" value="" name="id">
                        </form>

                        <span class="error" id="error_32"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        <button id="add_images" type="button" class="btn btn-success" style="margin-left: 5px">Salvar</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">
            $('.pg-<?=$thisPage?>').addClass('active');

            $(".add_images").click(function () {
                var id = $(this).attr("data-id");
                $("#images-id").val(id);

                $("#modal_images").modal();
            });

            $("#add_images").click(function () {
                $("#error_32").html("");
                var error = 0;
                var $fileUpload = $("#imagens");

                if (parseInt($fileUpload.get(0).files.length) > 10) {
                    $("#error_32").html("<p>* máximo de 10 arquivos permitidos</p>");
                    error++;
                }
                if ($fileUpload.val().length === 0) {
                    $("#error_32").html("<p>* nenhuma imagem selecionada</p>");
                    error++;
                }

                if (error === 0) {
                    var form_data = new FormData($("#form_images")[0]);

                    $.ajax({
                        url: 'ajax/noticia.php?action=img_novo',
                        data: form_data,
                        type: "POST",
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $("#loading-images").html('<div style="margin-bottom: 10px" class="alert alert-info alert-dismissible" role="alert">Cadastrando imagem(ns). Por favor, aguarde...</div>');
                        },
                        success: function (result) {
                            switch (result) {
                                case 'reload':
                                    window.location = "inc/logout.php";
                                    break;
                                case 'done':
                                    window.location = "noticias.php?status=success";
                                    break;
                                default:
                                    window.location = "noticias.php?status=error";
                                    break;
                            }
                        }
                    });
                }
            });

            $(".del").click(function () {
                var id = $(this).attr("data-id");

                $("#confirm-id").val(id);
                $("#modal_confirm").modal();
            });

            $("#del").click(function () {
                var form_data = {
                    id: $('#confirm-id').val()
                };

                $.ajax({
                    url: 'ajax/noticia.php?action=del',
                    data: form_data,
                    type: "POST",
                    success: function (result) {
                        switch (result) {
                            case 'reload':
                                window.location = "inc/logout.php";
                                break;
                            case 'done':
                                window.location = "noticias.php?status=success";
                                break;
                            default:
                                window.location = "noticias.php?status=error";
                                break;
                        }
                    }
                });
            });

        </script>
    </body>
</html>