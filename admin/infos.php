<?php
require 'inc/protect.php';
$thisPage = 'infos';
// $thisChild = 'info1';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Informações Gerais</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result">
                    <?php
                    if (isset($_GET['status'])) {
                        if ($_GET['status'] == 'success') {
                            ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                Ação efetuada com sucesso!
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

                <?php
                $sql = "select * from infos where id = 1";
                $resultado = $content->sql($sql);
                if ($resultado) {
                    $num_rows = $content->num_rows($resultado);
                    if ($num_rows === 1) {
                        while ($row = $content->fetch($resultado)) {
                            ?>
                            <form id="form" accept-charset="utf-8">

                                <label for="endereco"><h4>Endereço </h4></label>
                                <textarea id="endereco" class="form-my-control form-control" name="endereco"><?php echo str_replace("<br>", "\n", $content->limpaEcho($content->decodificar($row['endereco']))) ?></textarea><br>

                                <label for="telefones"><h4>Telefones <span class="obs_admin">(rodapé - um telefone por linha)</span></h4></label>
                                <textarea id="telefones" class="form-my-control form-control" name="telefones"><?php echo str_replace("<br>", "\n", $content->limpaEcho($content->decodificar($row['telefones']))) ?></textarea><br>


                                <label for="atendimento"><h4>Telefone principal <span class="obs_admin">(cabeçalho - apenas um telefone)</span></h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['telefone'])) ?>" id="atendimento" type="text" class="form-my-control form-control" name="telefone"><br>

                                <label for="email"><h4>E-mail<span class="error" id="error_1"></span></h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['email'])) ?>" type="text" id="email" class="form-my-control form-control" name="email"><br>

                                <label for="facebook"><h4>Facebook</h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['facebook'])) ?>" id="facebook" type="text" class="form-my-control form-control" name="facebook"><br>

                                <label for="linkedin"><h4>LinkedIn</h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['linkedin'])) ?>" id="linkedin" type="text" class="form-my-control form-control" name="linkedin"><br>

                                <label for="instagram"><h4>Instagram</h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['instagram'])) ?>" id="instagram" type="text" class="form-my-control form-control" name="instagram"><br>

                                <label for="youtube"><h4>Youtube</h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['youtube'])) ?>" id="youtube" type="text" class="form-my-control form-control" name="youtube"><br>

                                <label for="iframe"><h4>Google Maps</h4></label>
                                <textarea id="iframe" class="form-my-control form-control" name="iframe"><?php echo str_replace("<br>", "\n", $purifier->purify($row['iframe'])) ?></textarea><br>
                                <br>


                                <div class="clearfix"></div><br>
                                <button class="btn btn-success">Salvar</button>

                            </form>
                            <?php
                        }
                    } else {
                        echo "<div class='text-center'>Conteúdo não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/verimail.jquery.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">

            $('.pg-<?=$thisPage?>').addClass('active');

            $("#form").submit(function (e) {
                $("#admin_result").html("");
                $(".error").html("");
                var error = 0;

                var verimail = new Comfirm.AlphaMail.Verimail();
                verimail.verify($("#email").val(), function (status) {
                    if (status < 0) {
                        $("#error_1").html("* você deve informar um e-mail válido");
                        error++;
                    }
                });

                if (error === 0) {
                    var form_data = new FormData($(this)[0]);

                    $.ajax({
                        url: 'ajax/infos.php?action=alter1',
                        data: form_data,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                            $.smoothScroll({
                                scrollTarget: '#admin_result',
                                offset: -20,
                                speed: 200
                            });
                        },
                        success: function (result) {
                            switch (result) {
                                case 'reload':
                                    window.location = "inc/logout.php";
                                    break;
                                case 'done':
                                    window.location = "infos.php?status=success";
                                    break;
                                default:
                                    $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                    break;
                            }
                        }
                    });
                }

                e.preventDefault();
                e.unbind();
            });

        </script>
    </body>
</html>