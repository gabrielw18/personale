<?php
require 'inc/protect.php';

$thisPage = 'home';

?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Página Inicial</h3>
            <div class="clearfix"></div>

            <!--<div class="content_admin"></div>-->
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">
            $('.pg-<?=$thisPage?>').addClass('active');
        </script>
    </body>
</html>