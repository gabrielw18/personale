-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Mar-2018 às 11:33
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_make`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `link` text NOT NULL,
  `imagem` varchar(300) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `ativo` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `titulo`, `link`, `imagem`, `ordem`, `ativo`) VALUES
(1, '3VjrwBotL4JkUB66QStEQAzV5gASLtxmFcLB4gAj3njpL4Z03kqDmOj9UXj14oZh34qNHVxqLBjjsBZhYk6D5oUM34JoCqtV3ttoqtutU4t', 'cxA2Kqt=eojVQStGtAuJsTZ1icZyoAe6mcjV2qu', '3c9K4fg==l6BUOjMUVi7LoUoZVuGLqZ9Bgq7oqU6tntGQSq4G6fXq4tVB6uhqr3kocekBdjmaK6B54A4L4UuUtq2XAZVfAugtV6', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `subtitulo` text CHARACTER SET latin1 NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(30) NOT NULL,
  `ordem` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `id` int(11) NOT NULL,
  `imagem` varchar(300) NOT NULL,
  `titulo` text NOT NULL,
  `empresa` text NOT NULL,
  `texto` text NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `imagem`, `titulo`, `empresa`, `texto`, `ordem`) VALUES
(1, 'iUtZDML==l6BUOjMUVi7LoUXngqNHqZJ3Be9oVU6tBtumOe6tnZmittJQtAhuqtqsKAztdjoZAuNaK6zLVUBUtq2XWqVUOZgsKt', 'LOBZV3O==lqNYqt73tf6L4jheKtWuqUkUVA9UdtHUKtTfcffo6zBmVZmi4tt16zq5dU', 'TMoX3ko=VB6hsOAk9TqJUnZ1q4tFLgtGBgjVsKUutoL2Xg8fQgqGLVu', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE `imagens` (
  `id` int(11) NOT NULL,
  `imagem` text NOT NULL,
  `galeria` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `produto` int(11) DEFAULT NULL,
  `infos` int(11) DEFAULT NULL,
  `portfolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL,
  `endereco` text NOT NULL,
  `telefone` text NOT NULL,
  `telefones` text NOT NULL,
  `email` text NOT NULL,
  `horario` text NOT NULL,
  `facebook` text NOT NULL,
  `linkedin` text CHARACTER SET latin1 NOT NULL,
  `instagram` text CHARACTER SET latin1 NOT NULL,
  `youtube` text CHARACTER SET latin1 NOT NULL,
  `endereco1` text CHARACTER SET latin1 NOT NULL,
  `telefones1` text CHARACTER SET latin1 NOT NULL,
  `iframe` text NOT NULL,
  `iframe1` text CHARACTER SET latin1,
  `email1` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `infos`
--

INSERT INTO `infos` (`id`, `endereco`, `telefone`, `telefones`, `email`, `horario`, `facebook`, `linkedin`, `instagram`, `youtube`, `endereco1`, `telefones1`, `iframe`, `iframe1`, `email1`) VALUES
(1, 'LOBZV3O=etxmentkUSjMUBZ1i6Z6UoAum6j9fWL144xgtgtmaKAU1cxsBdjHigtkUdZtQAZWi4tJ1V6huBJLocj75txJBgtFU6jMtqfu3B6zfXtcQkffL4A9UdtH9q6D5486BrxWftxoYtAT1qUqocLWqdt0tTtWHVj9UVUcfotHf4qhhOZAmcjY5cUc9t6umdiJGcxvLtxouoZyUtuf5Tj', 'QEx683Y==ltF54tLLttmuBtz1VqBBWZfLVAiUcZFtntRL4itQr3XuttJmqxVG6AcoO6', 'aBtmjwB=u4xhs6AG5ojtfrjb5tUhlkAB1nq4GOUdiotV5kthar8jQgAyUOtj9t6G9SA6BrxRBBxYB6LqLntk1OtW54Z0QWumiO6Mtneo3Btz1VqBBWZfLVAiUcZFtntRL4itQr3XuttJmV8j3V6DQBt', 'uXf8ALN==i6t4Wj93n8kfcthVg6NHnezL4AsUdjoiTtw5otJ5gqdwcUzsOuw5B6k94qroXU53KqyUdjVUoj13tq2XAe6mcjV2qu', 'ioZBgfN==i6t4Wj93n8kf6q2XWZyUtuf5Tj', 'tWed33J=VojjUktE5VA4scZSLtZv3TtF9cx6Lq614otK9kAB9TZ7U4JjUc6f4k6DsoJ6Brem5txmncLkoV8jUqAoq6tS5rtt9OjfLVJ54tq2XWqVUOZgsKt', 'jctBFxc=n4xc4OtNuojMsTZ1q4tF5kty96tVLV6wsBJhfkAmuoeVLnLjU4jFUt6DsoAMtB8ouojVG6AT3k8jUqioq6tS5rtt9OjfLVJ54tq2Xg6stkA65tq', '4ULK9Kq==iqDLqj65gty5d32XA8zsOUfBAq', 'LOBZV3O==i6tUdtJ1OAs5o32XrtzQrxg3nL', 'jctBFxc==i6ysB3tsOiztdL2XgJ6oOUV96q', 'uXf8ALN==i6t4Wj93n8kf6q2Xg6stkA65tq', ' <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d110927.63135770916!2d-51.126912239863124!3d-29.676614384820038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951941aae20ada5b%3A0xdeac9da563b42254!2sR.+Em%C3%ADlio+Blos+Segundo%2C+235+-+Paulista%2C+Campo+Bom+-+RS%2C+93700-000!5e0!3m2!1spt-BR!2sbr!4v1522131225106" width="600" height="450" frameborder="0" style="border:0;"></iframe>', '', '4ULK9Kq==iqtsot43VU05oL2Xg8fQgqGLVu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE `institucional` (
  `id` int(11) NOT NULL,
  `texto` longtext NOT NULL,
  `resumo` text CHARACTER SET latin1,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `imagem` varchar(300) NOT NULL,
  `texto3` longtext CHARACTER SET latin1 NOT NULL,
  `texto4` longtext CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `texto`, `resumo`, `texto1`, `texto2`, `imagem`, `texto3`, `texto4`) VALUES
(1, '<p>A Makesystem no momento presente está em constante evolução, aderindo tecnologias avançadas para agregar mais produtividade e atender às frequentes mudanças do mercado, ajudando as empresas em todo o Brasil a gerenciarem melhor e mais facilmente os seus negócios.</p>', '<p>Situada na cidade de São Leopoldo, a Makesystem iniciou suas atividades em 2010, com o propósito de desenvolver sistemas de gestão para call center Vivo. A solução proposta trouxe tantos benefícios que a empresa se estruturou e expandiu sua atuação para todo o Brasil, agregando qualidade, produtividade e automatizando uma série de procedimentos manuais.<br />A solução possui integração com sistemas da Vivo, com sistemas da NET e estamos desenvolvendo integrações para outras operadoras, além de integração direta com centrais telefônicas.<br />Atualmente estamos muito bem localizados na região metropolitana de Porto Alegre, em uma sala com 200 m2, estruturados com áreas de desenvolvimento (gerentes de projetos, analistas, desenvolvedores e DBAs), área de suporte, área comercial e administrativa.<br />Estamos em fase de desenvolvimento de nossa solução full web, solução para plataformas mobile bem como um módulo totalmente voltado para apoiar as tomadas de decisão, com tecnologia de BI (Business Intelligence).<br />A Makesystem no momento presente está em constante evolução, aderindo tecnologias avançadas para agregar mais produtividade e atender às frequentes mudanças do mercado, ajudando as empresas em todo o Brasil a gerenciarem melhor e mais facilmente os seus negócios.</p>', '<p>Morbi lobortis vel erat sit amet egestas. Cras sem metus, rutrum eget lectus vel, eleifend consectetur lorem. Aliquam erat volutpat. Sed a erat a orci accumsan viverra. Nam gravida tempor maximus. In est tellus, ultrices facilisis tempus sit amet, sollicitudin eget risus. Ut at augue sem.</p>', '<p>Morbi lobortis vel erat sit amet egestas. Cras sem metus, rutrum eget lectus vel, eleifend consectetur lorem. Aliquam erat volutpat. Sed a erat a orci accumsan viverra. Nam gravida tempor maximus. In est tellus, ultrices facilisis tempus sit amet, sollicitudin eget risus. Ut at augue sem.</p>', '', '<p>Morbi lobortis vel erat sit amet egestas. Cras sem metus, rutrum eget lectus vel, eleifend consectetur lorem. Aliquam erat volutpat. Sed a erat a orci accumsan viverra. Nam gravida tempor maximus. In est tellus, ultrices facilisis tempus sit amet, sollicitudin eget risus. Ut at augue sem.</p>', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `tipo` text NOT NULL,
  `imagem` text CHARACTER SET latin1,
  `data` date NOT NULL,
  `dia` varchar(300) NOT NULL,
  `resumo` text NOT NULL,
  `texto` longtext NOT NULL,
  `ativo` int(2) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `categoria` int(2) DEFAULT NULL,
  `novo` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `tipo`, `imagem`, `data`, `dia`, `resumo`, `texto`, `ativo`, `ordem`, `keywords`, `categoria`, `novo`) VALUES
(1, '3ce4XfA=Vtt1F6uT9StgUBZ1i6Z0Q4uBLttAGO66Lti2XWqVUOZgsKt', '', '3ce4XfA==it6sXtq94th3txXngqNHqZ9fre7oqU69otuQK6JtqZWi4tV1qAtLVfqo6Z7BdjmZgq65BqfmcxtUoL2XAtoqtutU4t', '0000-00-00', '', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. ', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>', 1, 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `pagina` text NOT NULL,
  `titulo` text NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `seo`
--

INSERT INTO `seo` (`id`, `pagina`, `titulo`, `keywords`, `description`) VALUES
(1, 'home', '4ULK9KqtU4Uz5txh2qLqQAztLBehCqtVUtf4snJV2cL', 'jctBFxc==l6BUdxMoc87BdL2XrtzQrxg3nL', '4ULK9Kq==hqTGc8f3VqLoqq2XAtoqtutU4t'),
(2, 'empresa', 'uXf8ALN=YoUX9OqBmdUfUBZ1q4tFLgtGBgjVsKUutoL2Xgtt16zq5dU', '4ULK9Kq==htq5XAjL4q9t6q2Xg8fQgqGLVu', '9yZjAVS==i6t4Wj93n8kf6q2Xg8j3V6DQBt'),
(3, 'produtos', '3Y3T581=YoU1FdqBQTxzUtZzoVZ53Kty96uVGce5Ltq2Xg8j3V6DQBt', 'xBOUXZW==iqDLqj65gty5d32XAxVG6AcoO6', 'iUtZDML==ltF54tLLttmutq2XWqVUOZgsKt'),
(4, 'contato', 'iUtZDML=eoj6s66q1XjfUBZ1iOZ53KqyUdjVUoj1ttq2XAZc4nU9GcL', 'TMoX3ko==htGBWUq3B3y56q2XAxVG6AcoO6', 'tWed33J==ltDQKjfoOqyto32XA8jUtLzQA6');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `senha` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `senha`) VALUES
(1, 'contato@makesystem.com.br', 'a38a60ef830ybfe89deasa43d6ddcf03e4bcd141d4ad1c48822ff7c8161c23d0274a3d186b0637837d5d741e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infos`
--
ALTER TABLE `infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institucional`
--
ALTER TABLE `institucional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `infos`
--
ALTER TABLE `infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `institucional`
--
ALTER TABLE `institucional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
