<?php
require 'inc/protect.php';
$thisPage = 'banners';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Banners</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <?php
                if (isset($_GET['status'])) {
                    if ($_GET['status'] == 'success') {
                        ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Ação efetuada com sucesso!
                        </div>
                        <?php
                    } elseif ($_GET['status'] == 'error') {
                        ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            Erro ao efetuar ação. Tente novamente mais tarde.
                        </div>
                        <?php
                    }
                }
                ?>

                <a href="bannerNovo.php" class="btn btn-success">Novo Banner</a><br><br>

                <?php
                $sql = "SELECT id, titulo, link, imagem, ativo FROM banners ORDER BY ordem";
                $resultado = $content->sql($sql);
                if ($resultado) {
                    $num_rows = $content->num_rows($resultado);
                    if ($num_rows > 0) {
                        echo "<h4>Os banners são exibidos nesta ordem.</h4>";
                        while ($row = $content->fetch($resultado)) {
                            ?>
                            <div style="position: relative; border-top: 1px dotted #777; padding-top: 20px; margin-top: 20px; width: 900px">
                                <a href="bannerAlter.php?id=<?php echo $row['id'] ?>" class="btn btn-info btn-sm">Editar</a>
                                <button data-id="<?php echo $row['id'] ?>"  data-arquivo="<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" class="del btn btn-danger btn-sm">Excluir</button>
                                <br><br>
                                <?php
                                if (!empty($content->limpaEcho($content->decodificar($row['titulo'])))) {
                                    echo "<h4 style='margin: 0'>Título: <span class='obs_admin'>" . $content->limpaEcho($content->decodificar($row['titulo'])) . "</span></h4><br>";
                                }
                                if (!empty($content->limpaEcho($content->decodificar($row['link'])))) {
                                    echo "<p style='margin: 0'>Link: <span class='obs_admin'>" . $content->limpaEcho($content->decodificar($row['link'])) . "</span></p><br>";
                                }
                                ?>
                                <div class="radius_5 shadow_2" style="border: 5px solid #fff">
                                    <img src="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" style="width: 100%">
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo "<div class='text-center'>Nenhum cadastro encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <div id="modal_confirm" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-admin">
                        <h4 class="modal-title title-admin">Confirmação</h4>
                    </div>
                    <div class="modal-body body-admin">
                        <p>Você tem certeza que deseja excluir isto?<br>Essa ação não pode ser desfeita.</p>
                        <input type="hidden" id="confirm-id" value="">
                        <input type="hidden" id="confirm-imagem" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        <button id="del" type="button" class="btn btn-danger" style="margin-left: 5px">Excluir</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">
            $('.pg-<?=$thisPage?>').addClass('active');

            $(".del").click(function () {
                var id = $(this).attr("data-id");
                var arquivo = $(this).attr("data-arquivo");

                $("#confirm-id").val(id);
                $("#confirm-imagem").val(arquivo);
                $("#modal_confirm").modal();
            });

            $("#del").click(function () {
                var form_data = {
                    id: $('#confirm-id').val(),
                    arquivo: $('#confirm-imagem').val()
                };

                $.ajax({
                    url: 'ajax/banner.php?action=del',
                    data: form_data,
                    type: "POST",
                    success: function (result) {
                        switch (result) {
                            case 'reload':
                                window.location = "inc/logout.php";
                                break;
                            case 'done':
                                window.location = "banners.php?status=success";
                                break;
                            default:
                                window.location = "banners.php?status=error";
                                break;
                        }
                    }
                });
            });

        </script>
    </body>
</html>