<?php
require 'inc/protect.php';
$thisPage = 'noticias';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Editar notícia</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>

                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <?php
                if (!empty($_GET['id']) and ctype_digit((string) $_GET['id'])) {
                    $sql = "SELECT id, titulo, tipo, imagem, data, texto, resumo, ativo, ordem, keywords FROM noticias WHERE id = " . $_GET['id'];
                    $resultado = $content->sql($sql);
                    if ($resultado) {
                        $num_rows = $content->num_rows($resultado);
                        if ($num_rows > 0) {
                            $row = $content->fetch($resultado);
                            ?>
                            <form id="form" accept-charset="utf-8">

                                <label for="titulo"><h4>Título <span class="error" id="error_2"></span></h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['titulo'])) ?>" type="text" id="titulo" class="form-my-control form-control" name="titulo"><br>

                                <label><h4>Imagem <span class="obs_admin">(dimensões mínimas: largura 1200px - altura 649px)</span> <span class="error" id="error_2"></span></h4></label>
                                <div id="img_2" style="padding-top: 4px">
                                    <img src="../img/upload/thumb/<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" class="img-thumbnail" style="max-width: 700px"><br><br>
                                    <input type="button" class="btn btn-info btn-sm" id="alter_img" value=" alterar ">
                                </div>
                                <div id="img_1" class="dhidden">
                                    <input type="file" name="imagem" id="imagem">
                                </div><br>

                                <label for="data"><h4>Data <span class="error" id="error_3"></span></h4></label>
                                <input value="<?php echo $content->dateToData($row['data']) ?>" type="text" id="data" class="form-my-control form-control" name="data" style="width: 200px; min-width: 200px"><br>

                                <label for="resumo"><h4>Resumo <span class="error"></span></h4></label>
                                <textarea id="resumo" name="resumo" class="form-my-control form-control"><?php echo $purifier->purify($row['resumo']) ?></textarea><br>

                                <label for="keywords"><h4>Palavras chaves <span class="error"></span></h4></label>
                                <textarea id="keywords" name="keywords" class="form-my-control form-control"><?php echo $purifier->purify($row['keywords']) ?></textarea><br>

                                <label for="texto"><h4>Texto <span class="error" id="error_4"></span></h4></label>
                                <textarea id="texto" class="tinymce form-my-control form-control"><?php echo $purifier->purify($row['texto']) ?></textarea><br>

                                <label for="ativo"><h4>Ativo</h4></label>
                                <select id="ativo" name="ativo" class="form-my-control form-control" style="width: 100px; min-width: 100px">
                                <?php if($purifier->purify($row['ativo']) == 0){ ?>
                                    <option value="0" selected="selected">Inativo</option>
                                    <option value="1">Ativo</option>
                                <?php }else{ ?>
                                    <option value="0">Inativo</option>
                                    <option value="1" selected="selected">Ativo</option>
                                <?php } ?>
                                </select><br>

                                <label for="ordem"><h4>Ordem</h4></label>
                                <input type="text" value="<?= $purifier->purify($row['ordem']) ?>" id="ordem" class="form-my-control form-control numeric" name="ordem" style="width: 100px; min-width: 100px"><br>

                                <label><h4>Galeria de Fotos</h4> <a href="noticiaOrdena.php?id=<?=$row['id']?>" class="btn btn-success"><span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;Ordenar Imagens</a><br><br></label>
                                <div id="img_container">
                                    <?php
                                    $sql_fotos = "select id, imagem from imagens where noticia = " . $row['id'] ." ORDER BY ordem";
                                    $resultado_fotos = $content->sql($sql_fotos);
                                    if ($resultado_fotos) {
                                        $num_rows_fotos = $content->num_rows($resultado_fotos);
                                        if ($num_rows_fotos > 0) {
                                            while ($row_fotos = $content->fetch($resultado_fotos)) {
                                                ?>
                                                <div class="alter_img">
                                                    <a href='javascript:void(0)' class='alter_close' onclick='window.del_img(<?php echo $row_fotos['id'] . ", " . $row['id'] . ", \"" . $content->limpaEcho($content->decodificar($row_fotos['imagem'])) . "\""; ?>);'>
                                                        <img src='img/no.png'/>
                                                    </a>
                                                    <div>
                                                        <a href="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="gallery-item" data-title="" title="clique para ampliar" class="gallery-item">
                                                            <img src="../img/upload/thumb/<?php echo $content->limpaEcho($content->decodificar($row_fotos['imagem'])) ?>" class="shadow_2"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            echo "Nenhuma imagem cadastrada.";
                                        }
                                    } else {
                                        echo "Nenhuma imagem encontrada.";
                                    }
                                    ?>
                                    <div class="clearfix"></div><br>
                                </div>

                                <div class="clearfix"></div><br>
                                <input type="hidden" name="id" value="<?php echo $content->limpaEcho($row['id']) ?>">
                                <button class="btn btn-success">Salvar</button>

                            </form>
                            <?php
                        } else {
                            echo "<div class='text-center'>Cadastro não encontrado.</div>";
                        }
                    } else {
                        echo "<div class='text-center'>Cadastro não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>ja -Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/moment-with-locales.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/jquery.mask.min.js"></script>
        <script type="text/javascript" src="TinyMCE/tinymce.min.js"></script>
        <script type="text/javascript">

                                $('.pg-<?=$thisPage?>').addClass('active');

                                function del_img(id, post, imagem) {
                                    $.ajax({
                                        type: "POST",
                                        url: "ajax/noticia.php?action=img_del",
                                        data: {id: id, post: post, imagem: imagem},
                                        success: function (result) {
                                            $("#img_container").html(result);
                                        }
                                    });
                                }

                                $("#form").submit(function (e) {
                                    $("input").blur();
                                    $("textarea").blur();
                                    $("#admin_result").html("");
                                    $(".error").html("");
                                    var error = 0;

                                    if ($("#imagem").val() !== "" && !$("#imagem").val().match(/(?:gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/)) {
                                        $("#error_2").html("* este arquivo não é uma imagem");
                                        error++;
                                    }

                                    if ($("#titulo").val() === "") {
                                        $("#error_2").html("* campo obrigatório");
                                        error++;
                                    }

                                    var data = moment($("#data").val(), "DD/MM/YYYY");
                                    if (!data.isValid()) {
                                        $("#error_3").html("* campo obrigatório");
                                        error++;
                                    }

                                    var texto = tinyMCE.get('texto').getContent();
                                    if (texto === "") {
                                        $("#error_4").html("* campo obrigatório");
                                        error++;
                                    }

                                    if (error === 0) {
                                        var form_data = new FormData($(this)[0]);
                                        form_data.append('texto', texto);

                                        $.ajax({
                                            url: 'ajax/noticia.php?action=alter',
                                            data: form_data,
                                            type: "POST",
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            beforeSend: function () {
                                                $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                                                $.smoothScroll({
                                                    scrollTarget: '#admin_result',
                                                    offset: -20,
                                                    speed: 200
                                                });
                                            },
                                            success: function (result) {
                                                switch (result) {
                                                    case 'reload':
                                                        window.location = "inc/logout.php";
                                                        break;
                                                    case 'done':
                                                        window.location = "noticias.php?status=success";
                                                        break;
                                                    default:
                                                        $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                                        break;
                                                }
                                            }
                                        });
                                    }

                                    e.preventDefault();
                                    e.unbind();
                                });

                                tinymce.init({
                                    selector: '.tinymce',
                                    autoresize_min_height: 0,
                                    language: "pt_BR",
                                    theme: 'modern',
                                    menu: {},
                                    plugins: [
                                        'autoresize advlist autolink lists link image charmap hr anchor',
                                        'searchreplace code fullscreen',
                                        'media save table contextmenu directionality',
                                        'paste textcolor colorpicker textpattern imagetools jbimages'
                                    ],
                                    toolbar1: 'code | undo redo | styleselect | bold italic underline strikethrough removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink | jbimages media | table',
                                    image_advtab: true,
                                    imagetools_toolbar: "imageoptions",
                                    relative_urls: false
                                });

                                $(function () {
                                    $('#data').datetimepicker({
                                        language: 'pt-BR',
                                        pickTime: false,
                                        format:'DD/MM/YYYY'
                                    });
                                });

                                $(document).ready(function () {
                                    $('#data').mask('00/00/0000', {clearIfNotMatch: true});
                                });

                                $('.gallery-item').magnificPopup({
                                    tClose: 'Fechar',
                                    tLoading: '',
                                    type: 'image',
                                    mainClass: 'mfp-zoom-in',
                                    removalDelay: 200,
                                    closeBtnInside: false,
                                    image: {
                                        titleSrc: 'data-title',
                                        tError: 'A <a href="%url%">imagem</a> não pode ser carregada.'
                                    },
                                    gallery: {
                                        enabled: true,
                                        tPrev: 'Anterior',
                                        tNext: 'Próxima',
                                        tCounter: ''
                                    },
                                    callbacks: {
                                        imageLoadComplete: function () {
                                            var self = this;
                                            setTimeout(function () {
                                                self.wrap.addClass('mfp-image-loaded');
                                            }, 16);
                                        },
                                        beforeClose: function () {
                                            $(".mfp-arrow").hide();
                                        },
                                        close: function () {
                                            this.wrap.removeClass('mfp-image-loaded');
                                        }
                                    }
                                });

        </script>
    </body>
</html>