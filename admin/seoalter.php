<?php
require 'inc/protect.php';
$thisPage = 'seo';

?>
<?php include 'inc/topo.php'; ?>
        <div class="wrap_admin">
            <h3 class="header_admin">Editar SEO</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>

                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <?php
                if (!empty($_GET['id']) and ctype_digit((string) $_GET['id'])) {
                    $sql = "SELECT id, pagina, titulo, keywords, description FROM seo WHERE id = " . $_GET['id'];
                    $resultado = $content->sql($sql);
                    if ($resultado) {
                        $num_rows = $content->num_rows($resultado);
                        if ($num_rows > 0) {
                            $row = $content->fetch($resultado);
                            ?>
                            <form id="form" accept-charset="utf-8">

                                <label for="pagina"><h4>Página <span class="error" id="error_3"></span></h4></label>
                                <input value="<?php echo $content->limpaEcho(($row['pagina'])) ?>" type="text" id="pagina" class="form-my-control form-control" name="pagina"><br>

                                <label for="titulo"><h4>Título <span class="error" id="error_3"></span></h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['titulo'])) ?>" type="text" id="titulo" class="form-my-control form-control" name="titulo"><br>

                                <label for="keywords"><h4>Palavras Chaves</h4></label>
                                <textarea id="keywords" name="keywords" class="form-my-control form-control"><?php echo $content->limpaEcho($content->decodificar($row['keywords'])) ?></textarea><br>

                                <label for="description"><h4>Descrição <span class="error" id="error_5"></span></h4></label>
                                <textarea id="description" name="description" class="form-my-control form-control"><?php echo $content->limpaEcho($content->decodificar($row['description'])) ?></textarea><br>

                                <div class="clearfix"></div><br>
                                <input type="hidden" name="id" value="<?php echo $content->limpaEcho($row['id']) ?>">
                                <button class="btn btn-success">Salvar</button>

                            </form>
                            <?php
                        } else {
                            echo "<div class='text-center'>Cadastro não encontrado.</div>";
                        }
                    } else {
                        echo "<div class='text-center'>Cadastro não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/color.js"></script>
        <script type="text/javascript" src="../js/easing.js"></script>
        <script type="text/javascript" src="../js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="TinyMCE/tinymce.min.js"></script>
        <script type="text/javascript">

                    $('.pg-<?=$thisPage?>').addClass('active');

                    $("#form").submit(function (e) {
                        $("#admin_result").html("");
                        $(".error").html("");
                        var error = 0;

                        if (error === 0) {
                            var form_data = new FormData($(this)[0]);

                            $.ajax({
                                url: 'ajax/seo.php?action=alter',
                                data: form_data,
                                type: "POST",
                                cache: false,
                                contentType: false,
                                processData: false,
                                beforeSend: function () {
                                    $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                                    $.smoothScroll({
                                        scrollTarget: '#admin_result',
                                        offset: -20,
                                        speed: 200
                                    });
                                },
                                success: function (result) {
                                    switch (result) {
                                        case 'reload':
                                            window.location = "inc/logout.php";
                                            break;
                                        case 'done':
                                            window.location = "seo.php?status=success";
                                            break;
                                        default:
                                            $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                            break;
                                    }
                                }
                            });
                        }

                        e.preventDefault();
                        e.unbind();
                    });


        </script>
    </body>
</html>