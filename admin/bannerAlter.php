<?php
require 'inc/protect.php';
$thisPage = 'banners';
?>

    <?php include 'inc/topo.php'; ?>
        <div class="wrap_admin">
            <h3 class="header_admin">Editar Banner</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result"></div>

                <button class="btn btn-success" onclick="window.history.back()"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Voltar</button><br><br>

                <?php
                if (!empty($_GET['id']) and ctype_digit((string) $_GET['id'])) {
                    $sql = "SELECT id, titulo, subtitulo, link, imagem, ordem, ativo FROM banners WHERE id = " . $_GET['id'];
                    $resultado = $content->sql($sql);
                    if ($resultado) {
                        $num_rows = $content->num_rows($resultado);
                        if ($num_rows > 0) {
                            $row = $content->fetch($resultado);
                            ?>
                            <form id="form" accept-charset="utf-8">

                                <label><h4>Imagem de Fundo <span class="obs_admin">(dimensão: 1920px x 695px)</span> <span class="error" id="error_2"></span></h4></label>
                                <div id="img_2" style="padding-top: 4px">
                                    <img src="../img/upload/resize/<?php echo $content->limpaEcho($content->decodificar($row['imagem'])) ?>" class="img-thumbnail" style="width: 900px; max-width: 900px"><br><br>
                                    <input type="button" class="btn btn-info btn-sm" id="alter_img" value=" alterar ">
                                </div>
                                <div id="img_1" class="dhidden">
                                    <input type="file" name="imagem" id="imagem">
                                </div><br>

                                <label for="titulo"><h4>Titulo <span class="error" id="error_1"></span></h4></label>
                                <input value="<?= $content->limpaEcho($content->decodificar($row['titulo'])) ?>" type="text" id="titulo" class="form-my-control form-control" name="titulo"><br>


                                <label for="subtitulo"><h4>Subtitulo </h4></label>
                                <input value="<?= $content->limpaEcho($content->decodificar($row['subtitulo'])) ?>" type="text" id="subtitulo" class="form-my-control form-control" name="subtitulo"><br>

                                <label for="link"><h4>Link</h4></label>
                                <input value="<?php echo $content->limpaEcho($content->decodificar($row['link'])) ?>" type="text" id="texto" class="form-my-control form-control" name="link"><br>

                                <label for="ativo"><h4>Ativo</h4></label>
                                <select id="ativo" name="ativo" class="form-my-control form-control" style="width: 100px; min-width: 100px">
                                <?php if ($content->limpaEcho($row['ativo']) == 1) { ?>
                                    <option value="0">Inativo</option>
                                    <option value="1" selected="selected">Ativo</option>
                                <?php }else{ ?>
                                    <option value="0" selected="selected">Inativo</option>
                                    <option value="1">Ativo</option>
                                <?php } ?>
                                </select><br>

                                <label for="ordem"><h4>Ordem</h4></label>
                                <input value="<?php echo $content->limpaEcho($row['ordem']); ?>" type="text" id="ordem" class="form-my-control form-control numeric" name="ordem" style="width: 100px; min-width: 100px"><br>

                                <div class="clearfix"></div><br>
                                <input value="<?php echo $content->limpaEcho($row['id']) ?>" type="hidden" name="id">
                                <button class="btn btn-success">Salvar</button>

                            </form>
                            <?php
                        } else {
                            echo "<div class='text-center'>Cadastro não encontrado.</div>";
                        }
                    } else {
                        echo "<div class='text-center'>Cadastro não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">

            $('.pg-<?=$thisPage?>').addClass('active');

            $("#form").submit(function (e) {
                $("#admin_result").html("");
                $(".error").html("");
                var error = 0;

                if ($("#titulo").val() === "") {
                    $("#error_1").html("* campo obrigatório");
                    error++;
                }

                if ($("#imagem").val() !== "" && !$("#imagem").val().match(/(?:gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/)) {
                    $("#error_2").html("* este arquivo não é uma imagem");
                    error++;
                }

                if (error === 0) {
                    var form_data = new FormData($(this)[0]);

                    $.ajax({
                        url: 'ajax/banner.php?action=alter',
                        data: form_data,
                        type: "POST",
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                            $.smoothScroll({
                                scrollTarget: '#admin_result',
                                offset: -20,
                                speed: 200
                            });
                        },
                        success: function (result) {
                            switch (result) {
                                case 'reload':
                                    window.location = "inc/logout.php";
                                    break;
                                case 'done':
                                    window.location = "banners.php?status=success";
                                    break;
                                default:
                                    $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                    break;
                            }
                        }
                    });
                }

                e.preventDefault();
                e.unbind();
            });

        </script>
    </body>
</html>