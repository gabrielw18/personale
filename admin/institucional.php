<?php
require 'inc/protect.php';
$thisPage = 'institucional';
?>

    <?php include 'inc/topo.php'; ?>

        <div class="wrap_admin">
            <h3 class="header_admin">Texto Institucional</h3>
            <div class="clearfix"></div>

            <div class="content_admin">

                <div id="admin_result">
                    <?php
                    if (isset($_GET['status'])) {
                        if ($_GET['status'] == 'success') {
                            ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                Ação efetuada com sucesso!
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

                <?php
                $sql = "select texto, texto1, texto2, texto3, texto4, resumo, imagem from institucional where id = 1";
                $resultado = $content->sql($sql);
                if ($resultado) {
                    $num_rows = $content->num_rows($resultado);
                    if ($num_rows === 1) {
                        while ($row = $content->fetch($resultado)) {
                            ?>

                            <form id="form" accept-charset="utf-8">

                                <h4>Quem Somos<span class="error" id="error_1"></span></h4><br>
                                <label for="resumo">Texto Institucional</label>
                                <textarea id="resumo" class="tinymce form-my-control form-control">
                                    <?php echo $purifier->purify($row['resumo']); ?>
                                </textarea>

                                <div class="clearfix"></div><br>

                                <div class="clearfix"></div><br>

                                <label for="texto1">Sequencia Quem Somos</label>
                                <textarea id="texto1" class="tinymce form-my-control form-control">
                                    <?php echo $purifier->purify($row['texto1']); ?>
                                </textarea>

                                <div class="clearfix"></div><br><br>

                                <button class="btn btn-success">Salvar</button>

                            </form>

                            <?php
                        }
                    } else {
                        echo "<div class='text-center'>Conteúdo não encontrado.</div>";
                    }
                } else {
                    echo "<div class='text-center'>Erro ao buscar conteúdo.</div>";
                }
                ?>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="js/jquery.smooth-scroll.js"></script>
        <script type="text/javascript" src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="TinyMCE/tinymce.min.js"></script>
        <script type="text/javascript">

            $('.pg-<?=$thisPage?>').addClass('active');

            tinymce.init({
                selector: '.tinymce',
                autoresize_min_height: 0,
                language: "pt_BR",
                theme: 'modern',
                menu: {},
                plugins: [
                    'autoresize advlist autolink lists link image charmap hr anchor',
                    'searchreplace code fullscreen',
                    'media save table contextmenu directionality',
                    'paste textcolor colorpicker textpattern imagetools jbimages'
                ],
                toolbar1: 'code | undo redo | bold italic underline strikethrough removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink table | jbimages media',
                image_advtab: true,
                imagetools_toolbar: "imageoptions",
                relative_urls: false
            });

            $("#form").submit(function (e) {
                $("#admin_result").html("");
                $(".error").html("");
                var error = 0;

                var resumo = tinyMCE.get('resumo').getContent();
                // var empresa = tinyMCE.get('empresa').getContent();
                var texto1 = tinyMCE.get('texto1').getContent();
                // var texto2 = tinyMCE.get('texto2').getContent();
                // var texto3 = tinyMCE.get('texto3').getContent();


                if (error === 0) {
                    var form_data = new FormData($(this)[0]);
                    form_data.append('resumo', resumo);
                    // form_data.append('texto', empresa);
                    form_data.append('texto1', texto1);
                    // form_data.append('texto2', texto2);
                    // form_data.append('texto3', texto3);

                    $.ajax({
                        url: 'ajax/institucional.php?action=alter',
                        data: form_data,
                        type: "POST",
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $("#admin_result").html('<div class="alert alert-info alert-dismissible" role="alert">Salvando...</div>');
                            $.smoothScroll({
                                scrollTarget: '#admin_result',
                                offset: -20,
                                speed: 200
                            });
                        },
                        success: function (result) {
                            switch (result) {
                                case 'reload':
                                    window.location = "inc/logout.php";
                                    break;
                                case 'done':
                                    window.location = "institucional.php?status=success";
                                    break;
                                default:
                                    $("#admin_result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                    break;
                            }
                        }
                    });
                }

                e.preventDefault();
                $(this).unbind(e);
            });

        </script>
    </body>
</html>