<?php
require 'inc/access.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Acesso Restrito</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, follow">

        <!--[if lte IE 8]>
          <script type="text/javascript">
            window.location = "../inc/update.php";
          </script>
        <![endif]-->

        <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="../img/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

        <link href='bootstrap/css/bootstrap.css' rel="stylesheet" type="text/css"/>
        <link href='bootstrap/css/bootstrap-nonresponsive.css' rel="stylesheet" type="text/css"/>
        <link href='css/style.css' rel="stylesheet" type="text/css"/>
    </head>
    <body class="body-login">
        <form id="log-form" accept-charset="utf-8">
            <div class="login_table">
                <img src="img/logo.png"/>
                <table>
                    <tbody>
                        <tr>
                            <td ><br><h3>Dados de Login</h3></td>
                        </tr>
                        <tr>
                            <td >
                                <div id="log-result"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><input id="log-email" type="text" placeholder="E-mail" class="form-control"></td>
                        </tr>
                        <tr>
                            <td><input id="log-password" type="password" placeholder="Senha" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-info pull-right" type="submit" value="entrar"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript" src="js/easing.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/verimail.jquery.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript">

            $("#log-form").submit(function (e) {
                $("#log-result").html("");
                var error = 0;

                if ($("#log-password").val() === "") {
                    $("#log-result").html('<div class="alert alert-danger alert-dismissible" role="alert">Preencha o campo "senha" corretamente</div>');
                    error++;
                }

                var verimail = new Comfirm.AlphaMail.Verimail();
                verimail.verify($("#log-email").val(), function (status) {
                    if (status < 0) {
                        $("#log-result").html('<div class="alert alert-danger alert-dismissible" role="alert">Preencha o campo "e-mail" corretamente</div>');
                        error++;
                    }
                });

                if (error === 0) {
                    var form_data = {
                        email: $('#log-email').val(),
                        senha: $('#log-password').val()
                    };

                    $.ajax({
                        url: 'ajax/login.php?action=login',
                        data: form_data,
                        method: "POST",
                        beforeSend: function () {
                            $("#log-result").html('<div class="alert alert-info alert-dismissible" role="alert">Verificando dados...</div>');
                        },
                        success: function (result) {
                            switch (result) {
                                case 'access-granted':
                                    $("#log-result").html('<div class="alert alert-success alert-dismissible" role="alert">Efetuando login...</div>');
                                    window.location = "index.php";
                                    break;
                                default:
                                    $("#log-result").html('<div class="alert alert-danger alert-dismissible" role="alert">' + result + '</div>');
                                    break;
                            }
                        }
                    });
                }
                e.preventDefault();
            });

        </script>
    </body>
</html>